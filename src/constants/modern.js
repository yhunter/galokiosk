

import Achieves__Contests from "../screens/modern/AchievesContests";
import Achieves__Rewards from "../screens/modern/AchievesRewards";

import Culture__Events from "../screens/modern/CultureEvents";
import Culture__Mind from "../screens/modern/CultureMind";
import Culture__Sports from "../screens/modern/CultureSport";
import Culture__Veterans from "../screens/modern/CultureVeterans";
import Culture__Youth from "../screens/modern/CultureYouth";

import Culture from "../screens/modern/Culture";


export const modern = [ //Персоналии
    //{
    //    "name": "",
    //    "photo": "/img/",
    //    "text": ""
    //},
    [],
    [],
    [],
    [
        {
            "name": "Победы в конкурсах",
            "component": (<Achieves__Contests />)      
        },
        {
            "name": "Награды Института",
            "component": (<Achieves__Rewards />)       
        }
    ],
    [
        {
            "name": "Корпоративная культура",
            "component": (<Culture />)      
        },
        {
            "name": "Корпоративные мероприятия",
            "component": (<Culture__Events />)      
        },
        {
            "name": "Интеллектуальные конкурсы",
            "component": (<Culture__Mind />)      
        },
        {
            "name": "Спортивные мероприятия",
            "component": (<Culture__Sports />)      
        },
        {
            "name": "Молодежная организация",
            "component": (<Culture__Youth />)      
        }
        //{
        //    "name": "Ветеранская организация",
        //    "component": (<Culture__Veterans />)      
        //}
    ]
]