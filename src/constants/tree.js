//Файл структуры

//Импортируем компоненты для страниц
import Page__Galurgy from "../screens/pages/Galurgy";
import Page__Floatation from "../screens/pages/Floatation";
import Page__Mining from "../screens/pages/Mining";
import Page__Project from "../screens/pages/Project";
import Page__Bim from "../screens/pages/Bim";
import Page__Books from "../screens/pages/Books";
import Page__Chronology from "../screens/pages/Chronology";
import Page__Expeditions from "../screens/pages/Expeditions";

import Page__HistoryKeps from "../screens/pages/HistoryKeps";
import Page__HistoryLab from "../screens/pages/HistoryLab";
import Page__HistoryWW from "../screens/pages/HistoryWW";
import Page__HistoryReconstr from "../screens/pages/HistoryReconstr";
import Page__HistoryGrow from "../screens/pages/HistoryGrow";
import Page__HistoryBorn from "../screens/pages/HistoryBorn";
import Page__HistoryCentury from "../screens/pages/HistoryCentury";
import Page__HistoryReunion from "../screens/pages/HistoryReunion";
import Page__HistoryStructure from "../screens/pages/HistoryStructure";

import Page__ModernAchiev from "../screens/pages/ModernAchiev";
import Page__ModernCulture from "../screens/pages/ModernCulture";
import Page__ModernInfo from "../screens/pages/ModernInfo";
import Page__ModernManagement from "../screens/pages/ModernManagement";
import Page__ModernStrategy from "../screens/pages/ModernStrategy";

import Page__GalleryHistory from "../screens/pages/GalleryHistory";
import Page__GalleryModern from "../screens/pages/GalleryModern";

import Directions__Author from "../screens/pages/DirectionsAuthor";
import Directions__Monitoring from "../screens/pages/DirectionsMonitoring";


const path = require("path"); 
const folder = "galokiosk";

export const pathstr = ('file:///'+path.dirname(process.execPath).split(folder)[0]+folder+'/').replace(/\\/g,'/');




export const tree = {
    "folder": folder,
    "ch1": {
        "title": "История",
        "route": "/submenu/ch1",
        "items": [
            {
                "title": "От КЕПС к Соляной лаборатории: <br/>1918-1931",
                "route": "/page/ch1/0",
                "component": (<Page__HistoryKeps />)
            },
            {
                "title": "От Соляной лаборатории к ВИГу:  <br/>1931-1935",
                "route": "/page/ch1/1",
                "component": (<Page__HistoryLab />)
            },
            {
                "title": "ВИГ в годы ВОВ: <br/>1941-1945",
                "route": "/page/ch1/2",
                "component": (<Page__HistoryWW />)
            },
            {
                "title": "ВНИИГ: восстановление института. <br/>1946-1960 гг",
                "route": "/page/ch1/3",
                "component": (<Page__HistoryReconstr />)
            },
            {
                "title": "ВНИИГ: бурный рост. <br/>1960-1971 гг",
                "route": "/page/ch1/4",
                "component": (<Page__HistoryGrow />)
            },
            {
                "title": "ВНИИГ —  1972-1985. Филиалы",
                "route": "/page/ch1/5",
                "component": (<Page__HistoryBorn />)
            },
            {
                "title": "ВНИИГ — на пороге нового века: <br/>1992-2016",
                "route": "/page/ch1/6",
                "component": (<Page__HistoryCentury />)
            },

            {
                "title": "Воссоединение АО «ВНИИ Галургии»",
                "route": "/page/ch1/7",
                "component": (<Page__HistoryReunion />)
            },
            {
                "title": "Структура современного института",
                "route": "/page/ch1/8",
                "component": (<Page__HistoryStructure />)
            },
            {
                "title": "Летопись",
                "route": "/page/ch1/chronology"
            }
        ]
    },
    "ch2": {
      "title": "Персоналии",
      "route": "/submenu/ch2",
      "items": [
          {
              "title": "Основоположники",
              "route": "/page/ch2/persons/0"
          },
          {
              "title": "Ученые 1930-1940-х годов, руководители экспедиций по разведке солей",
              "route": "/page/ch2/persons/1"
          },
          //{
          //  "title": "Ученые в годы ВОВ, руководители",
          //  "route": "/page/ch2/persons/2"
          //},
          {
              "title": "Ученые, проектировщики 1940-1960-х",
              "route": "/page/ch2/persons/2"
          },
          {
              "title": "Ученые и проектировщики ВНИИга<br /> в 1960-1980-х",
              "route": "/page/ch2/persons/3"
          },
          {
              "title": "Директора институтов в 1990-2020-х,<br /> персоналии сотрудников.",
              "route": "/page/ch2/persons/4"
          },
          {
              "title": "Руководство современного института <nobr>АО «ВНИИ Галургии»</nobr>",
              "route": "/page/ch2/persons/5"
          }          
      ]
  },
  "ch3": {
    "title": "Технологии",
    "route": "/submenu/ch3",
    "items": [
        {
            "title": "Галургия",
            "route": "/page/ch3/0",
            "component": (<Page__Galurgy />)
        },
        {
            "title": "Флотация",
            "route": "/page/ch3/1",
            "component": (<Page__Floatation />)
        },
        {
            "title": "Технологии добычи",
            "route": "/page/ch3/2",
            "component": (<Page__Mining />)
        },
        {
            "title": "От кульманов к компьютерам  и САПР",
            "route": "/page/ch3/3",
            "component": (<Page__Project />)
        },
        {
            "title": "Технологии BIM",
            "route": "/page/ch3/4",
            "component": (<Page__Bim />)
        },
        {
            "title": "Издания института",
            "route": "/page/ch3/5",
            "component": (<Page__Books />)
        },
        {
            "title": "Технологии в хронологии",
            "route": "/page/ch3/chronology"
        },

        {
            "title": "Хронология экспедиций",
            "route": "/page/ch3/expeditions",
            "component": (<Page__Expeditions />)
        }
    ]
  },
  "ch4": {
    "title": "Проекты",
    "route": "/submenu/ch4",
    "items": [
        {
            "title": "Объекты проектирования",
            "route": "/page/ch4/projects"
        },
        {
            "title": "Карта расположения проектов",
            "route": "/page/ch4/map"
        }
    ]
  },
  "ch5": {
    "title": "Экспонаты",
    "route": "/submenu/ch5",
    "items": [
        {
            "title": "От кульмана к виртуальному центру",
            "route": "/page/ch5/0"
        },
        {
            "title": "Научные инструменты",
            "route": "/page/ch5/1"
        },
        {
            "title": "Образцы солей, минералы",
            "route": "/page/ch5/1"
        }
    ]
  },
  "ch6": {
    "title": "Современный институт",
    "route": "/submenu/ch6",
    "items": [
        {
            "title": "Информация об институте",
            "route": "/page/ch6/0",
            "component": (<Page__ModernInfo />)
        },
        {
            "title": "Руководство",
            "route": "/page/ch6/1",
            "component": (<Page__ModernManagement />)
        },
        {
            "title": "Кандидаты наук",
            "route": "/page/ch6/candidates/0",
        },
        
        {
            "title": "Стратегия",
            "route": "/page/ch6/2",
            "component": (<Page__ModernStrategy />)
        },
        {
            "title": "Достижения",
            "route": "/page/ch6/modern/3"
        },
        {
            "title": "Корпоративная культура",
            "route": "/page/ch6/modern/4"
        }
    ]
  },
  "ch7": {
    "title": "Направления деятельности",
    "route": "/submenu/ch7",
    "items": [

        {
            "title": "Проектная деятельность",
            "route": "/page/ch7/modern/0"
        },
        {
            "title": "Научная деятельность",
            "route": "/page/ch7/modern/1"
        },
        {
            "title": "Мониторинг и обследование технического состояния конструкций",
            "route": "/page/ch7/2",
            "component": (<Directions__Monitoring />)
        },
        {
            "title": "Авторский надзор",
            "route": "/page/ch7/3",
            "component": (<Directions__Author />)
        }
    ]
  },
  "ch8": {
    "title": "Фотогалерея",
    "route": "/submenu/ch7",
    "items": [
        {
            "title": "История",
            "route": "/page/ch8/0",
            "component": (<Page__GalleryHistory />)
        },
        {
            "title": "Современность",
            "route": "/page/ch8/1",
            "component": (<Page__GalleryModern />)
        }
    ]
  }
}

