export const pointsData = [ //Точки на карте
    {
        "pointLabel": "ВНИИГ",
        "lat": 59,
        "lng": 43.2,
        "pointColor": "#fff",
        "pointRadius": 0,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": 1.5,
        "labelAltitude": .07,
        "content": "АО «ВНИИ Галургии» — научно-исследовательский и&nbsp;проектный институт в&nbsp;области разведки, добычи и&nbsp;переработки горно-химического сырья, объединивший научных и&nbsp;проектных специалистов в&nbsp;Перми и&nbsp;Санкт-Петербурге"
    },
    {
        "pointLabel": "Ливия",
        "lat": 27.25,
        "lng": 17.47,
        "pointColor": "#ff3333",
        "pointRadius": .7,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": 1,
        "labelAltitude": 0,
        "arcColor": ["red", "blue"],
        "arcStroke": .5,
        "content": "Ливия, город Марада, гос. предприятие. Научные исследования в области получения металлического магния"

    },
    {
        "pointLabel": "Узбекистан",
        "lat": 42.35,
        "lng": 61.56,
        "pointColor": "#fff",
        "pointRadius": .7,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": 1,
        "labelAltitude": 0,
        "arcColor": ["red", "blue"],
        "arcStroke": .5,
        "content": "Узбекистан, Тюбегатанское месторождение — Горнодобывающий комплекс Дехканабадского завода калийных удобрений"

    },
    {
        "pointLabel": "Канада",
        "lat": 51,
        "lng": -92.7, 
        "pointColor": "#ff3333",
        "pointRadius": .7,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": 1,
        "labelAltitude": 0,
        "arcColor": ["red", "gold"],
        "arcStroke": .5,
        "content": "Канада, город Саскатун, «Nutrien Potash». Обмен опытом в области процессов производства калийных и NPK удобрений.<br /><br /> «Can Pacific Potash Inc.». Обмен опытом подземного выщелачивани калийных солей. <br /><br />Город Делта, «Erize». Обмен опытом по вопросам использования  колонных флотомашин в производстве KCl"

    },
    {
        "pointLabel": "США",
        "lat": 41.5,
        "lng": -87.4, 
        "pointColor": "#ff3333",
        "pointRadius": .7,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": 1,
        "labelAltitude": 0,
        "arcColor": ["red", "gold"],
        "arcStroke": .5,
        "content": "США, город Плимут, штат Минесота, «Mosaic Company». Обмен опытом борьбы с притоками воды в рудники. <br /><br />Город Денвер, штат Колорадо, «Intrepid Potash Inc.»  Обмен опытом бассейного производства калийных и калийно — магниевых солей. <br /><br />Город Эверт, штат Мичиган, «Michigan Potash & Salt Comp.». Обмен опытом  создания нового предприятия по производству калийных удобрений и соляных рассолопрмыслов"

    },
    {
        "pointLabel": "Исландия",
        "lat": 64.5,
        "lng": -17.6, 
        "pointColor": "#ff3333",
        "pointRadius": .7,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": 1,
        "labelAltitude": 0,
        "arcColor": ["red", "blue"],
        "arcStroke": .5,
        "content": "Исландия, город Рейкьявик, гос. предприятие. Обмен опытом в области использования возобновляемых источников энергии"

    },
    {
        "pointLabel": "Франция",
        "lat": 46.5,
        "lng": 2.3, 
        "pointColor": "#ff3333",
        "pointRadius": .7,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": 1,
        "labelAltitude": 0,
        "arcColor": ["red", "blue"],
        "arcStroke": .5,
        "content": "Франция, город Миссон, «Fertinagro». Обмен опытом научно-исследовательских работ по повышению качества минеральных удобрений"

    },
    {
        "pointLabel": "Германия",
        "lat": 51.3,
        "lng": 9.6, 
        "pointColor": "#ff3333",
        "pointRadius": .7,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": 1,
        "labelAltitude": 0,
        "arcColor": ["red", "blue"],
        "arcStroke": .5,
        "content": "Германия, город Кассель, «К+S AG», Обмен опытом в области процессов производства калийных и калийно - магниевых солей. <br /><br />Город Мюльхайм-на-Руре, «Thyssen Schachtbau GmbH», Обмен опытом по строительству шахтных стволов<br /><br />Город Хаттинген, «Кőppern», Консультации по приобретению технологического оборудования для проведения исследований по кондиционированию калийных удобрений"

    },
    //{
    //    "pointLabel": "Ирак",
    //    "lat": 33.2,
    //    "lng": 44.2, 
    //    "pointColor": "#ff3333",
    //    "pointRadius": .7,
    //    "pointAltitude": .5,
    //    "color": "#fff",
    //    "labelSize": 1,
    //    "labelAltitude": 0,
    //    "arcColor": ["red", "blue"],
    //    "arcStroke": .5,
    //    "content": ""

    //},
    {
        "pointLabel": "Иран",
        "lat": 32.3,
        "lng": 54.2, 
        "pointColor": "#ff3333",
        "pointRadius": .7,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": 1,
        "labelAltitude": 0,
        "arcColor": ["red", "blue"],
        "arcStroke": .5,
        "content": "Иран, город Бендер-Хомейни, завод «Рази» (гос.сектор). Консультации по вопросам процессов производства и применения калийных удобрений"

    },
    {
        "pointLabel": "Иордания",
        "lat": 30.8,
        "lng": 36.7, 
        "pointColor": "#ff3333",
        "pointRadius": .4,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": .5,
        "labelAltitude": 0,
        "arcColor": ["red", "blue"],
        "arcStroke": .5,
        "content": "Иордания, город Амман, «Arab Potash Co». Обмен опытом бассейного производства калия и производственных процессов<br /><br /> «Kemapco». Обмен опытом производства нитрата калия"

    },
    {
        "pointLabel": "Египет",
        "lat": 27,
        "lng": 29, 
        "pointColor": "#ff3333",
        "pointRadius": .7,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": 1,
        "labelAltitude": 0,
        "arcColor": ["red", "blue"],
        "arcStroke": .5,
        "content": "Египет, город Каир, «Egyptian Chemical Industries Company». Консультации по вопросам процессов производства и применения калийных удобрений"

    },
    {
        "pointLabel": "Израиль",
        "lat": 31.5,
        "lng": 34.4, 
        "pointColor": "#ff3333",
        "pointRadius": .4,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": .5,
        "labelAltitude": 0,
        "arcColor": ["red", "blue"],
        "arcStroke": .5,
        "content": "Израиль, город Тельавив, «ICL GROUP». Обмен опытом бассейного  производства калия и производственных процессов. <br /><br />Город Матам-Хайфа, «Haifa Group». Консультации по производству специальных сортов калийных удобрений"

    },
    {
        "pointLabel": "Китай",
        "lat": 32.5,
        "lng": 103.0, 
        "pointColor": "#ff3333",
        "pointRadius": .7,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": 1,
        "labelAltitude": 0,
        "arcColor": ["red", "blue"],
        "arcStroke": .5,
        "content": "Китай, город Голмуд, провинция Цинхай, «Qinghai Salt Lake Potash». Научные исследования по бассейному способу  производства калийных солей. <br /><br /> Гонконг, «Migao Corp.». Обмен опытом производства KNO<sub>3</sub> и K<sub>2</sub>SO<sub>4</sub>"

    },
    {
        "pointLabel": "Болгария",
        "lat": 42.5,
        "lng": 25.3, 
        "pointColor": "#ff3333",
        "pointRadius": .7,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": 1,
        "labelAltitude": 0,
        "arcColor": ["red", "blue"],
        "arcStroke": .5,
        "content": "Болгария, город Девня, «Agropolyhim Ad». Консультации по вопросам процессов производства и применения калийных удобрений"

    },
    {
        "pointLabel": "Туркменистан",
        "lat": 40,
        "lng": 55, 
        "pointColor": "#ff3333",
        "pointRadius": .7,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": 1,
        "labelAltitude": 0,
        "arcColor": ["red", "gold"],
        "arcStroke": .5,
        "content": "Туркменистан,  Балканский велаят, залив-лагуна Каспийского моря Кара́-Бога́з-Го́л — Карабогазская станция  изучения гидрохимического режима залива,  Карабогазский сульфатный комбинат (Карабогазсульфат)"

    },
    {
        "pointLabel": "Каспийская станция",
        "lat": 42.5,
        "lng": 47.4, 
        "pointColor": "#ff3333",
        "pointRadius": .4,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": .7,
        "labelAltitude": 0,
        "arcColor": ["red", "gold"],
        "arcStroke": .5,
        "content": "Россия, Республикак Дагестан, город Махачкала озеро большое Турали — Каспийская станция наблюдения за&nbsp;гидрохимическим режимом и&nbsp;испаряемостью рассолов озера"

    },
    {
        "pointLabel": "Крымская станция",
        "lat": 45.1,
        "lng": 33.3, 
        "pointColor": "#ff3333",
        "pointRadius": .4,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": .7,
        "labelAltitude": 0,
        "arcColor": ["red", "gold"],
        "arcStroke": .5,
        "content": "Российская Федерация, Республика Крым — Крымская станция гидрогеологического, геохимического изучения соляных озер"

    },
    {
        "pointLabel": "Казахстан",
        "lat": 46.5,
        "lng": 61.4, 
        "pointColor": "#ff3333",
        "pointRadius": .4,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": .7,
        "labelAltitude": 0,
        "arcColor": ["red", "gold"],
        "arcStroke": .5,
        "content": "Казахстан, Кызылординская область, Аральское море — Аральская станция станция гидрогеологического, геохимического изучения заливов"

    },

    //{
    //    "pointLabel": "Славгород",
    //    "lat": 53,
    //    "lng": 78.4, 
    //    "pointColor": "#ff3333",
    //    "pointRadius": .4,
    //    "pointAltitude": .5,
    //    "color": "#fff",
    //    "labelDotOrientation": 'top',
    //    "labelSize": .7,
    ///    "labelAltitude": 0

    //},
    {
        "pointLabel": "Березники, Соликамск",
        "lat": 59.4,
        "lng": 56.5, 
        "pointColor": "#ff3333",
        "pointRadius": .4,
        "pointAltitude": .5,
        "color": "#ccc",
        "labelSize": .7,
        "labelDotOrientation": 'top',
        "labelAltitude": 0,
        "arcColor": ["red", "gold"],
        "arcStroke": .5,
        "content": "Россия , Пермский край, г.&nbsp;Соликамск — Первый калийный комбинат им. X-летия Октябрьской революции, СКПРУ, 2-ой СКК, 3-ий СКК.<br /><br /> г.&nbsp;Березники  — Первый Березниковский калийный комбинат, Второй Березниковский калийный комбинат, БКРУ-3, Талицкий ГОК, Усть-Яйвинский рудник, Усольский ГОК"

    },
    {
        "pointLabel": "Алтайский край",//"Михайловская станция",
        "lat": 51.7,
        "lng": 79.8, 
        "pointRadius": .4,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": .7,
        "labelDotOrientation": 'bottom',
        "labelAltitude": 0,
        "arcColor": ["red", "gold"],
        "arcStroke": .5,
        "content": "Россия, Алтайский край, г.&nbsp;Славгород — Станция по изучению Кулундинских соляных озер. Михайловские содовые озера — Михайловская станция по&nbsp;изучению месторождения, Михайловский содовый завод. Посёлок  Степное Озеро — Кучукский сульфатный комбинат"

    },
    //{
    //    "pointLabel": "Озеро Степное",
    //    "lat": 51.8,
    //    "lng": 80.5, 
    //    "pointRadius": .4,
    //    "pointAltitude": .5,
    //    "color": "#fff",
    //    "labelSize": .7,
    //    "labelDotOrientation": 'right',
    //    "labelAltitude": 0  

    //},
    {
        "pointLabel": "Солигорск",
        "lat": 52.8,
        "lng": 27.5, 
        "pointColor": "#ff3333",
        "pointRadius": .4,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": .7,
        "labelAltitude": 0,
        "arcColor": ["red", "gold"],
        "arcStroke": .5,
        "content": "Беларусь г.&nbsp;Солигорск — 1-й Солигорский калийный комбинат, 2-ой Солигорский калийный комбинат, 4-ый Солигорский калийный комбинат, 4-ый Солигорский калийный комбинат, Старобинское месторождение (находящееся вблизи г.&nbsp;Солигорск)"
    },
    {
        "pointLabel": "Стебник",
        "lat": 49.3,
        "lng": 23.6, 
        "pointColor": "#ff3333",
        "pointRadius": .4,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": .7,
        "labelAltitude": 0,
        "arcColor": ["red", "gold"],
        "arcStroke": .5,
        "content": "Украина, г.&nbsp;Стебник — Стебниковский калийный комбинат"
    },
    {
        "pointLabel": "Белбажское",
        "lat": 57.2,
        "lng": 44, 
        "pointColor": "#ff3333",
        "pointRadius": .4,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": .7,
        "labelAltitude": 0,
        "arcColor": ["red", "gold"],
        "arcStroke": .5,
        "content": "Россия, Нижегородская область — Белбажское соляное месторождение"
    },
    {
        "pointLabel": "Сургут",
        "lat": 61.5,
        "lng": 73.4, 
        "pointColor": "#ff3333",
        "pointRadius": .4,
        "pointAltitude": .5,
        "color": "#117",
        "labelSize": .7,
        "labelAltitude": 0,
        "text": "Тут текст про Сургут",
        "arcColor": ["red", "gold"],
        "arcStroke": .5,
        "content": "Россия, г.&nbsp;Сургут, Полигон-ЛТД — проект «Полигона твердых бытовых и промышленных отходов (ТБПО)"
    },
    {
        "pointLabel": "Санкт-Петербург",
        "lat": 60,
        "lng": 30.3, 
        "pointColor": "#ff3333",
        "pointRadius": .4,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": .7,
        "labelAltitude": 0,
        "arcColor": ["red", "gold"],
        "arcStroke": .5,
        "content": "Россия, г.&nbsp;Санкт-Петербург — Балтийский  балкерный терминал"
    },
    {
        "pointLabel": "Камско-Устьинский рудник",
        "lat": 49.2,
        "lng": 55.2, 
        "pointColor": "#ff3333",
        "pointRadius": .4,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": .7,
        "labelAltitude": 0,
        "arcColor": ["red", "blue"],
        "arcStroke": .5,
        "content": "Россия, респ.&nbsp;Татарстан — Камско-Устьинский гипсовый рудник"
    },
    {
        "pointLabel": "Новомосковск",
        "lat": 54,
        "lng": 38.3, 
        "pointColor": "#ff3333",
        "pointRadius": .4,
        "pointAltitude": .5,
        "color": "#fff",
        "labelSize": .7,
        "labelAltitude": 0,
        "arcColor": ["red", "gold"],
        "arcStroke": .5,
        "content": "Россия, Московская область, г. Новомосковск — ОАО&nbsp;«Кнауф&nbsp;Гипс&nbsp;Новомосковск» "
    }   
    

    
];

