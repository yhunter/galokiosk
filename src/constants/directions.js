import Projects__Arch from "../screens/modern/ProjectsArch";
import Projects__Genplan from "../screens/modern/ProjectsGenplan";

import Projects__Hydro from "../screens/modern/ProjectsHydro";
import Projects__Electrotech from "../screens/modern/ProjectsElectrotech";
import Projects__Envir from "../screens/modern/ProjectsEnvir";
import Projects__Fire from "../screens/modern/ProjectsFire";
import Projects__Mech from "../screens/modern/ProjectsMech";
import Projects__MechTech from "../screens/modern/ProjectsMechTech";
import Projects__Org from "../screens/modern/ProjectsOrg";
import Projects__Safe from "../screens/modern/ProjectsSafe";
import Projects__Search from "../screens/modern/ProjectsSearch";
import Projects__Systems from "../screens/modern/ProjectsSystems";

import Science__GeoMech from "../screens/modern/ScienceGeoMech";
import Science__Aero from "../screens/modern/ScienceAero";
import Science__Auto from "../screens/modern/ScienceAuto";
import Science__Dry from "../screens/modern/ScienceDry";

import Science__Exam from "../screens/modern/ScienceExam";
import Science__Float from "../screens/modern/ScienceFloat";
import Science__Tech from "../screens/modern/ScienceTech";
import Science__Geology from "../screens/modern/ScienceGeology";

import Science__GeoPhis from "../screens/modern/ScienceGeoPhis";
import Science__Salts from "../screens/modern/ScienceSalts";

import Science__Ore from "../screens/modern/ScienceOre";
import Science__Hydro from "../screens/modern/ScienceHydro";
import Science__Mines from "../screens/modern/ScienceMines";
import Science__Plugs from "../screens/modern/SciencePlugs";



export const directions = [ //Персоналии
    //{
    //    "name": "",
    //    "photo": "/img/",
    //    "text": ""
    //},

    [
        {
            "name": "Архитектурно-строительный профиль",
            "component": (<Projects__Arch />)      
        },
        {
            "name": "Генплан и транспорт",
            "component": (<Projects__Genplan />)       
        },
        {
            "name": "Гидротехнические сооружения",
            "component": (<Projects__Hydro />)         
        },
        {
            "name": "Горно-механический профиль",
            "component": (<Projects__Mech />)       
        },
        {
            "name": "Инженерные изыскания",
            "component": (<Projects__Search />)         
        },
        {
            "name": "Инженерные системы",
            "component": (<Projects__Systems />)       
        },
        {
            "name": "Мероприятия по ГО и ЧС",
            "component": (<Projects__Safe />)         
        },
        {
            "name": "Механо-технологический профиль",
            "component": (<Projects__MechTech/>)       
        },
        {
            "name": "Охрана окружающей среды",
            "component": (<Projects__Envir />)         
        },
        {
            "name": "Пожарная безопасность",
            "component": (<Projects__Fire />)       
        },
        {
            "name": "Сметы и организация строительства",
            "component": (<Projects__Org />)         
        },
        {
            "name": "Электротехнический профиль",
            "component": (<Projects__Electrotech />)         
        }
    ],
    [
        {
            "name": "Геомеханика",
            "component": (<Science__GeoMech />)      
        },
        {
            "name": "Технология удобрений и&nbsp;солей",
            "component": (<Science__Salts />)      
        },
        {
            "name": "Строительство и эксплуатация шахтных стволов",
            "component": (<Science__Mines />)      
        },
        {
            "name": "Технология флотационного обогащения руд и&nbsp;реагенты",
            "component": (<Science__Float/>)      
        },
        {
            "name": "Геология",
            "component": (<Science__Geology />)      
        },
        {
            "name": "Исследования процессов сушки и&nbsp;грануляции",
            "component": (<Science__Dry />)      
        },
        {
            "name": "Геофизика",
            "component": (<Science__GeoPhis />)      
        },
        {
            "name": "Рудоподготовка",
            "component": (<Science__Ore />)      
        },
        {
            "name": "Технология и безопасность горных работ",
            "component": (<Science__Tech />)      
        },
        {
            "name": "Испытания и стандартизация продукции",
            "component": (<Science__Exam />)      
        },
        {
            "name": "Рудничная аэрология и&nbsp;газодинамика",
            "component": (<Science__Aero />)      
        },
        {
            "name": "Автоматизация управления технологическими процессами",
            "component": (<Science__Auto />)      
        },
        {
            "name": "Технология закладочных работ и&nbsp;тампонажных материалов",
            "component": (<Science__Plugs />)      
        },
        {
            "name": "Гидрогеология и охрана природных вод",
            "component": (<Science__Hydro />)      
        }
    ]
]