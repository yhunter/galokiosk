//Страница персоналии
import AppBar from '../components/AppBar';
import Shadow from '../components/Shadow';
import MainMenu from '../components/MainMenu';
import Timer from '../components/Timer';
import React, { useState, useRef, useEffect } from 'react';
import ScrollContainer from 'react-indiana-drag-scroll';

import Page__Test from "./pages/Test";

import {modern} from '../constants/modern.js';
import {directions} from '../constants/directions.js';
import {pathstr} from '../constants/tree';


const Modern = (props) => {
    
    const [menu, setMenu] = useState('hideMenu'); //Состояние меню
    const showMenu = () => { //Переключатель появления меню
        let menuStatus = (menu === "hideMenu") ? "showMenu" : "hideMenu";
        setMenu(menuStatus);
    }
    const [person, setPerson] = useState(0); //Состояние выбора персоны

    const selectPerson = (item) => { //Функция выбора персоны
        setPerson(item);
    }
    console.log(props.subpage);

    let content = directions;

    if (props.cat==="ch6") {
        content=modern;
    }


    const pageSelector = (page) => { //Если в pageTree есть нужный компонент, то выводим его. Если нет, то тестовую страницу (потом 404)
        if (content[props.subpage][page].component) {
            return(content[props.subpage][page].component);
        }
        else {
            return(<Page__Test />); 
        }

    }

    const header = [
        
        props.tree[props.cat],
        props.tree[props.cat].items[props.subpage]

    ];

    const imagesPath = pathstr+'data/persons/'; //Путь до фотографий

    const personImage = (img, alt) => { //Если есть адрес изображения, выводим. Если нет - null
        if (img!=="") {
            return (
                <>
                    <img className="modern__thumb" alt={alt} src={imagesPath+img} />
                </>
            );
        }
        else return null;
        
    }


    const container = useRef(null);

    const scrollToRef = () => {
        container.current.scrollTo(0, 0);
    }
    
    return(
        <div className="App">
            <section className={"page modern "+menu}>
                <AppBar header={header} menuCallBack={showMenu}  />
                
                <ScrollContainer   className="modern__field" innerRef={container}>

                {content[props.subpage][person] !== undefined && //Если массив персоналий этого раздела не пустой, то выводим первую карточку
                    <div className="modern__container">
                        <article className="modern__item">
                            <h2  dangerouslySetInnerHTML={{__html: content[props.subpage][person].name}}></h2>
                            {pageSelector(person)}
                        </article>
                    </div>
                }                  
                    
                    
                </ScrollContainer>
                <div className="modern__select">
                    <ScrollContainer  className="scroll-container" >
                    <ul>
                        {content[props.subpage].map((item, index) => (
                            <li data-index={index} key={index} className={index === person ? 'active' : ''}>                                    
                                <button onClick={(element) => {scrollToRef(); selectPerson(index); } }   dangerouslySetInnerHTML={{__html: item.name}}> 
                                    
                                </button>
                            </li>
                        ))}
                    </ul>
                    </ScrollContainer>
                </div>
                
                <div className="rightShadow">

                </div>
                <Shadow />
                <MainMenu />
                <Timer />
            </section>
        </div>
    );
}

export default Modern;