import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Achieves__Contests = (props) => {

    const images = [
        {
            "url": pathstr+"data/awards/galurgy-awards-1.jpg",
            "width": 1920,
            "height": 1440,
            "title": 'Команда ВНИИГ на Конкурсе Росатома'
        },
        {
            "url": pathstr+"data/awards/galurgy-awards-3.jpg",
            "width": 1920,
            "height": 1440,
            "title": 'Команда ВНИИГ в процессе выполнения задания'
        },
        {
            "url": pathstr+"data/awards/galurgy-awards-4.jpg",
            "width": 1920,
            "height": 1297,
            "title": 'Команда ВНИИГ в процессе выполнения задания'
        },
        {
            "url": pathstr+"data/awards/galurgy-awards-2.jpg",
            "width": 1920,
            "height": 1440,
            "title": 'Команда ВНИИГ — награждение'
        }
    ];

    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            
            <Spacer space="1rem" />
            <Gallery images={images} scale="200" color="transparent"  /> 
            <Text width="1000px">
               <p>Конкурс профессионального мастерства Росатома (2019 год). Номинация «Лучшая команда по информационному моделированию и проектированию») -2-ое место.</p>
               
            </Text>
            <Text width="1000px"> 
               <p>Первое место в номинации «Развитие кадрового потенциала». Конкурс Министерства промышленности и торговли Пермского края по итогам 2019 года.</p>
               <p>Первый Международный строительный чемпионат (2021 год) команда АО «ВНИИ Галургии» заняла Первое место в командной номинации «Информационное моделирование и проектирование»</p>
            </Text> 
            <Image url={pathstr+"data/awards/buildchamp.jpg"} thumb={pathstr+"data/awards/buildchamp.jpg"} width="1920" height="1440" title="Команда ВНИИГ — награждение" caption />
            <Spacer space="1rem" />
            <Text width="1000px"> 
            <center>
                <video controls  disablePictureInPicture controlsList="nodownload"  crossOrigin="anonymous">
                                <source src={pathstr+"data/awards/report.ogv"} type='video/webm'/>
                </video>
                <p><i>Репортаж Своё-ТВ</i></p>
                <video controls  disablePictureInPicture controlsList="nodownload"  crossOrigin="anonymous">
                                <source src={pathstr+"data/awards/project.ogv"} type='video/webm'/>
                </video>
                <p><i>Информационное моделирование и проектирование</i></p>
                <video controls  disablePictureInPicture controlsList="nodownload"  crossOrigin="anonymous">
                                <source src={pathstr+"data/awards/itog.ogv"} type='video/webm'/>
                </video>
                <p><i>Итоговый ролик МСЧ-2021</i></p>

                </center>
            </Text> 
            
            
            
        </>
    );
}

export default Achieves__Contests;