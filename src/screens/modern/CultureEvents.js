import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Culture__Events = (props) => {

    const images = [
        {
            "url": pathstr+"data/events/galurgy-firmday-1.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'День фирмы, Санкт-Петербург. 2019 г.'
        },
        {
            "url": pathstr+"data/events/galurgy-firmday-2.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'День фирмы, Санкт-Петербург. 2018 г.'
        },
        {
            "url": pathstr+"data/events/galurgy-firmday-3.jpg",
            "width": 1200,
            "height": 800,
            "title": 'День фирмы, Пермь. 2019 г.'
        },
        {
            "url": pathstr+"data/events/galurgy-firmday-4.jpg",
            "width": 1920,
            "height": 1282,
            "title": 'День фирмы, Пермь. 2019 г.'
        }
    ];
    const images2 = [
        {
            "url": pathstr+"data/events/ny2017.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'Новый год, Пермь. 2017 г.'
        },
        {
            "url": pathstr+"data/events/ny2019.jpg",
            "width": 1920,
            "height": 1269,
            "title": 'Новый год, Пермь. 2019 г.'
        }
    ];
    const images3 = [
        {
            "url": pathstr+"data/events/health2018.jpg",
            "width": 1920,
            "height": 1080,
            "title": 'День здоровья, Пермь. 2018'
        },
        {
            "url": pathstr+"data/events/tour2021.jpg",
            "width": 1920,
            "height": 1336,
            "title": 'Турслет, Пермь. 2021'
        }
    ];
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="1rem" />
            <Text width="1000px">
                <p>Одной из важных традиций коллектива института является проведение корпоративных мероприятий.</p>
            </Text>
            <Gallery images={images3} scale="300" color="transparent" caption />    
            <Spacer space="1rem" /> 
            <Text width="1000px">    
                <p>Среди традиционных мероприятий — внутренние чемпионаты по баскетболу, мини-футболу, турслеты, сплавы, выезды на природу, прогулки на теплоходе. </p>

                <h3>Профессиональные праздники</h3>
                <p>Отмечаются профессиональные праздники — а их в силу специфики Института много: День строителя, День шахтера, День химика. </p>

                <h3>День фирмы</h3>
                <p>Одним из самых ярких ежегодных событий является День фирмы. При подготовке к мероприятию в  институте царит особая атмосфера. Как правило, сотрудники выступают с постановочными номерами, демонстрируя свои таланты коллективу. </p>
            </Text>
            <Gallery images={images} scale="200" color="transparent" caption  />    
            <Spacer space="1rem" /> 
            <Text width="1000px">
                <p>На праздновании Дня фирмы всегда отмечаются заслуги лучших сотрудников: вручаются Медали за заслуги перед предприятием, фото самых достойных размещаются на Доске почета, используются такие формы признания заслуг, как Почетная грамота, Благодарность, Благодарственное письмо, Памятный адрес.</p>

                <h3>Новый год</h3>
            </Text>
            <Gallery images={images2} scale="300" color="transparent"  caption />    
            <Spacer space="1rem" /> 
            <Text width="1000px">
                <p>Феерично и креативно проходит также празднование Нового года. Много конкурсов и викторин, танцы и зажигательные номера от артистов, вкусные закуски и подарки – эти праздничные атрибуты надолго запоминаются сотрудникам, посетившим новогоднее мероприятие.</p>

                <h3>8 Марта</h3>
                <p>Особая атмосфера обычно царит на мероприятиях, посвященных Женскому дню 8 Марта. Мужчины галантно поздравляют коллег-женщин, готовят захватывающие концертные номера, записывают поздравительные ролики, вручают цветы.</p>
            </Text> 
            <ImageFull image={pathstr+"data/events/8march.jpg"} caption="8 Марта, Пермь. 2020 г."   />
            <Text width="1000px">
                {/*<center>
                    
                    <video controls  disablePictureInPicture controlsList="nodownload"  crossOrigin="anonymous">
                                    <source src={pathstr+"data/events/8march.ogv"} type='video/webm'/>
                    </video>
                    <p><i>Музыкальное поздравление с 8 марта</i></p>
                </center>  */}
                <h3>23 февраля</h3>
                {/*<center>
                    
                    <video controls  disablePictureInPicture controlsList="nodownload"  crossOrigin="anonymous">
                                    <source src={pathstr+"data/events/23feb.ogv"} type='video/webm'/>
                    </video>
                    <p><i>Поздравление с 23 февраля</i></p>
                </center> */}
            </Text> 
            
            <ImageFull image={pathstr+"data/events/23feb.jpg"} caption="23 февраля. 2019 г."  />

            
        </>
    );
}

export default Culture__Events;