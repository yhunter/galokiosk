import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Science__Ore = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="1rem" />
            <Text width="1000px">
                <p>Сектор рудоподготовки – структурное подразделение АО «ВНИИ Галургии», находится в г. Березники (Пермский край).</p>
                <ul>
                    <h3>Основные направления деятельности:</h3>
                    <li>научно-исследовательские и опытно-промышленные работы по определению условий снижения на стадии сухой классификации и дробления труднообогатимых фракций в сильвинитовой и карналлитовой руде;</li>
                    <li>выбор эффективного классифицирующего и дробильного оборудования, разработка схем его компоновки, исходные данные для проектирования; проведение испытаний;</li>
                    <li>выполнение работ по определению причин недостаточной эффективности технологических процессов, разработка рекомендаций по их повышению;</li>
                    <li>оказание научно-методической помощи по вопросам дробления и классификации руд и других материалов.</li>
                                        
                </ul>
            </Text> 
            
        </>
    );
}

export default Science__Ore;