import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Culture__Veterans = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="1rem" />
            <Text width="1000px">
                
                <ul>
                    <li>2011 г. Создана специализированная программа «Моделирование вентиляционных сетей рудников».</li>
                    <li>2015 г. В институте начали выполнять проекты с применением BIM-технологии</li>
                    <li>2015 г. АО «ВНИИ Галургии» заключило прямой договор с Autodesk</li>
                    <li>2017 г. Начато проектирование в объединенной информационной среде всех отделов с созданием трехмерных моделей проектируемых объектов. При построении внутренних графиков выполнения проектов учитываются и вопросы, связанные с чисто BIM-овской тематикой</li>
                    <li>2018 г. АО «ВНИИ Галургии» становится одним из разработчиков общероссийского BIM-стандарта «Промышленные объекты» разработано решение по получению смет напрямую из информационной модели. Техническое решение апробировано на одном из проектов</li>
                    <li>2019 г. Развивается горное (подземное) направление с применением BIM- технологий</li>
                    
                </ul>
            </Text> 
            <Spacer space="3rem" />
            <ImageFull image={pathstr+"data/bim1.jpg"} caption="BIM-модель шахтного поверхностного комплекса ствола №2 СКРУ-2 «Южный рудник»" noshadow   />
            <Spacer space="1rem" />
            <Text width="1000px">   
                <p>Цифровые технологии проектирования успешно внедряются на всех площадках института. Разрабатываются информационные модели реконструируемых и вновь возводимых объектов различной сложности на основе BIM-технологии. Создан и функционирует авторизованный учебный центр с площадками в Перми и Санкт-Петербурге, где слушатели имеют возможность получить сертификаты международного образца.</p>
                <p>Для внедрения BIM-технологии создано несколько специализированных курсов, которые действуют постоянно. Компетентные сотрудники отдела технологииинформационного моделирования проходят углубленные курсы, и дальше идет обучение сотрудников, которые занимаются проектированием. Вновь пришедшие в институт тоже имеют возможность пройти обучение.</p>
                <img src={pathstr+"data/bim2.jpg"} alt="BIM" />
            </Text>
            
        </>
    );
}

export default Culture__Veterans;