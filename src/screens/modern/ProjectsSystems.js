import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Projects__Systems = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
           
            <Text width="1000px">
                <p>Комплексный подход к проектированию систем инженерного обеспечения позволяет повысить энергоэффективность технологических процессов вновь проектируемых и действующих производств.</p>
                <ul>
                    <h3>Основные направления деятельности:</h3>
                    <li>инженерное обеспечение (сети и установки на них) отопление, вентиляция, кондиционирование, водоснабжение, канализация, ливневые стоки, теплоснабжение, газоснабжение, технологические трубопроводы;</li>
                    <li>системы оборотного водоснабжения;</li>
                    <li>системы противопожарного водоснабжения;</li>
                    <li>насосные станции;</li>
                    <li>очистные сооружения хозяйственно-бытовой, бытовой канализации и ливневых стоков;</li>
                    <li>системы пылеуборки;</li>
                    <li>системы аспирации и очистки воздуха, выбрасываемого в атмосферу от технологических установок;</li>
                    <li>системы воздушного охлаждения;</li>
                    <li>гидротранспорт солеотходов.</li>
                    
                </ul>
            </Text> 
            
            
        </>
    );
}

export default Projects__Systems;