import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Culture__Sports = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;

    const images = [
        {
            "url": pathstr+"data/events/soccer/galurgy-soccer-1.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Чемпионат по футболу, Пермь, 2016 г.'
        },
        {
            "url": pathstr+"data/events/soccer/galurgy-soccer-2.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Чемпионат по футболу, Пермь, 2016 г.'
        },
        {
            "url": pathstr+"data/events/soccer/galurgy-soccer-3.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Чемпионат по футболу, Пермь, 2016 г.'
        },
        {
            "url": pathstr+"data/events/soccer/galurgy-soccer-4.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Чемпионат по футболу, Пермь, 2016 г.'
        },
        {
            "url": pathstr+"data/events/soccer/galurgy-soccer-5.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Чемпионат по футболу, Пермь, 2016 г.'
        },
        {
            "url": pathstr+"data/events/soccer/galurgy-soccer-6.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Чемпионат по футболу, Пермь, 2017 г.'
        },
        {
            "url": pathstr+"data/events/soccer/galurgy-soccer-7.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Чемпионат по футболу, Пермь, 2017 г.'
        },
        {
            "url": pathstr+"data/events/soccer/galurgy-soccer-8.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Чемпионат по футболу, Пермь, 2017 г.'
        },
        {
            "url": pathstr+"data/events/soccer/galurgy-soccer-9.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Чемпионат по футболу, Пермь, 2017 г.'
        }
    ];
    const images2 = [
        {
            "url": pathstr+"data/events/pingpong/galurgy-pingpong-1.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Турнир по настольному теннису, СПб,  2017 г.'
        },
        {
            "url": pathstr+"data/events/pingpong/galurgy-pingpong-2.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Турнир по настольному теннису, СПб,  2017 г.'
        },
        {
            "url": pathstr+"data/events/pingpong/galurgy-pingpong-3.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Турнир по настольному теннису, СПб,  2017 г.'
        },
        {
            "url": pathstr+"data/events/pingpong/galurgy-pingpong-4.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Турнир по настольному теннису, СПб,  2017 г.'
        },
        {
            "url": pathstr+"data/events/pingpong/galurgy-pingpong-5.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Турнир по настольному теннису, СПб,  2017 г.'
        },
        {
            "url": pathstr+"data/events/pingpong/galurgy-pingpong-6.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Турнир по настольному теннису, СПб,  2017 г.'
        }
    ];
    const images3 = [
        {
            "url": pathstr+"data/events/basketball/galurgy-basketball-1.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Чемпионат по баскетболу, Пермь, 2017 г.'
        },
        {
            "url": pathstr+"data/events/basketball/galurgy-basketball-2.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Чемпионат по баскетболу, Пермь, 2017 г.'
        },
        {
            "url": pathstr+"data/events/basketball/galurgy-basketball-4.JPG",
            "width": 1280,
            "height": 1920,
            "title": 'Чемпионат по баскетболу, Пермь, 2017 г.'
        },
        {
            "url": pathstr+"data/events/basketball/galurgy-basketball-5.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Чемпионат по баскетболу, Пермь, 2017 г.'
        },
        {
            "url": pathstr+"data/events/basketball/galurgy-basketball-6.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Чемпионат по баскетболу, Пермь, 2017 г.'
        },
        {
            "url": pathstr+"data/events/basketball/galurgy-basketball-7.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Чемпионат по баскетболу, Пермь, 2017 г.'
        },
        {
            "url": pathstr+"data/events/basketball/galurgy-basketball-8.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Чемпионат по баскетболу, Пермь, 2017 г.'
        },
        {
            "url": pathstr+"data/events/basketball/galurgy-basketball-3.JPG",
            "width": 1280,
            "height": 1920,
            "title": 'Чемпионат по баскетболу, Пермь, 2017 г.'
        }
    ];
    const images4 = [
        {
            "url": pathstr+"data/events/tour/galurgy-tour-6.JPG",
            "width": 1024,
            "height": 768,
            "title": 'Турслет, СПб, 2019 г.'
        },
        {
            "url": pathstr+"data/events/tour/galurgy-tour-1.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Турслет, Пермь, 2018 г.'
        },
        {
            "url": pathstr+"data/events/tour/galurgy-tour-2.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Турслет, Пермь, 2018 г.'
        },
        {
            "url": pathstr+"data/events/tour/galurgy-tour-3.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Турслет, Пермь, 2018 г.'
        },
        {
            "url": pathstr+"data/events/tour/galurgy-tour-4.JPG",
            "width": 1920,
            "height": 885,
            "title": 'Турслет, Пермь, 2021 г.'
        },
        {
            "url": pathstr+"data/events/tour/galurgy-tour-5.JPG",
            "width": 1920,
            "height": 885,
            "title": 'Турслет, Пермь, 2021 г.'
        }
        
    ];
    const images5 = [
        {
            "url": pathstr+"data/events/cart/galurgy-cart-1.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Соревнования по картингу, г. Пермь, 2015 г.'
        },
        {
            "url": pathstr+"data/events/cart/galurgy-cart-2.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Соревнования по картингу, г. Пермь, 2015 г.'
        },
        {
            "url": pathstr+"data/events/cart/galurgy-cart-3.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Соревнования по картингу, г. Пермь, 2015 г.'
        }
    ];
    const images6 = [
        {
            "url": pathstr+"data/events/bowling/galurgy-bowling-1.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Турнир по боулингу, г. Пермь, 2014 г.'
        },
        {
            "url": pathstr+"data/events/bowling/galurgy-bowling-2.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Турнир по боулингу, г. Пермь, 2014 г.'
        },
        {
            "url": pathstr+"data/events/bowling/galurgy-bowling-3.JPG",
            "width": 1280,
            "height": 960,
            "title": 'Турнир по боулингу, СПб, 2019 г.'
        }
    ];
    const images7 = [
        {
            "url": pathstr+"data/events/paintball/galurgy-1.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Турнир по пейнтболу, СПб, 2019 г.'
        },
        {
            "url": pathstr+"data/events/paintball/galurgy-2.JPG",
            "width": 1920,
            "height": 1280,
            "title": 'Турнир по пейнтболу, СПб, 2019 г.'
        },
    ];
    return (
        <>
            <Spacer space="1rem" />
            <Text width="1000px">
                
                <p>В Институте ежегодно проводятся различные спортивные мероприятия.</p>

                <p>Инициаторами спортивных соревнований являются: Дирекция по персоналу, Профсоюзная организация и Совет Молодежи.</p>

                <p>Соревнования проводятся на открытых и закрытых площадках по следующим видам спорта:</p>

                <h3>Чемпионат по футболу</h3>
            </Text>
            <Gallery images={images} scale="220" color="transparent" caption />    
            <Spacer space="1rem" /> 
            <Text width="1000px">
                <h3>Турнир по настольному теннису</h3>
            </Text>
            <Gallery images={images2} scale="220" color="transparent" caption />    
            <Spacer space="1rem" /> 
            <Text width="1000px">
                <h3>Чемпионат по баскетболу</h3>
            </Text>
            <Gallery images={images3} scale="300" color="transparent" caption />    
            <Spacer space="1rem" /> 
            <Text width="1000px">    
                <h3>Туристический слет</h3>
            </Text>
            <Gallery images={images4} scale="200" color="transparent" caption />    
            <Spacer space="1rem" /> 
            <Text width="1000px">    
                <h3>Соревнования по картингу</h3>
            </Text>
            <Gallery images={images5} scale="250" color="transparent" caption />    
            <Spacer space="1rem" /> 
            <Text width="1000px">     
                <h3>Турнир по боулингу</h3>
            </Text>
            <Gallery images={images6} scale="250" color="transparent" caption />    
            <Spacer space="1rem" /> 
            <Text width="1000px">     
                <h3>Турнир по пейнтболу</h3>
            </Text>
            <Gallery images={images7} scale="250" color="transparent" caption />    
            <Spacer space="1rem" /> 
            <Text width="1000px">    
                <p>Для подготовки работников к соревнованиям предварительно проводятся тренировки команд.</p>
                <p>По результатам соревнований команды награждаются ценными призами и грамотами.</p>
                <p>В Институте приветствуется участие работников в городских спортивных мероприятиях (таких как «Пермский марафон»).</p>
                <p>Участие в соревнованиях способствуют укреплению физического и психологического здоровья работников и позволяет им эффективно взаимодействовать в команде.</p>
            </Text> 
            
            
        </>
    );
}

export default Culture__Sports;