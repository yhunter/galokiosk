import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Science__Tech = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="1rem" />
            <Text width="1000px">
                <p>Научно-исследовательская лаборатория технологии и безопасности горных работ является структурным подразделением научной части АО «ВНИИ Галургии».</p>
                <ul>
                    <h3>Основные направления деятельности:</h3>
                    <li>разработка безопасных технологий горных работ, их опытно-промышленные испытания;</li>
                    <li>разработка технологических регламентов ведения очистных работ, проходки подземных выработок, эксплуатации самоходного (нерельсового) оборудования в подземных выработках;</li>
                    <li>идентификация опасных производственных объектов с целью страхования гражданской ответственности за причинение вреда жизни, здоровью или имуществу других лиц и окружающей природной среде, а также регистрация объектов в Государственном реестре опасных производственных объектов;</li>
                    <li>разработка паспортов безопасности опасных объектов;</li>
                    <li>разработка энергосберегающих технологий проветривания горных выработок.</li>

                    
                </ul>
                <p>Лаборатория имеет лицензию Ростехнадзора на проведение экспертизы промышленной безопасности.</p>
            </Text> 
            
        </>
    );
}

export default Science__Tech;