import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Projects__Search = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            
            <Text width="1000px">
                <p>Институт, выполняя функции Генеральной проектной организации, осуществляет организацию проведения следующих инженерных изысканий</p>
                <ul>
                    <h3>Основные направления деятельности:</h3>
                    <li>инженерно-геодезические;</li>
                    <li>инженерно-геологические;</li>
                    <li>инженерно-экологические;</li>
                    <li>инженерно-гидрометеорологические;</li>
                    <li>инженерно-геофизические;</li>
                    <li>инженерно-геотехнические.</li>
                    
                </ul>
            </Text> 
            
        </>
    );
}

export default Projects__Search;