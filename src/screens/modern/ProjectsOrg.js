import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Projects__Org = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            
            <Text width="1000px">

                <ul>
                    <h3>Основные направления деятельности:</h3>
                    <li>проект организации строительства;</li>
                    <li>проект организации работ по сносу или демонтажу объектов капитального строительства;</li>
                    <li>разработка сметной документации.</li>                    
                </ul>
            </Text> 
            
        </>
    );
}

export default Projects__Org;