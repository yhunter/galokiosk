import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Science__Geology = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="1rem" />
            <Text width="1000px">
                <p>Изучение калийно-магниевых месторождений, обеспечение предприятий запасами калийно-магниевых руд и безопасность их добычи – основная задача подразделений геологического направления АО «ВНИИ Галургии» в Перми и Санкт-Петербурге.</p>
                <ul>
                    <h3>Основные направления деятельности:</h3>
                    <li>разведка и геолого-промышленная оценка месторождений природных солей; проектирование геологоразведочных работ и их научно-методическое сопровождение;</li>
                    <li>разработка технико-экономического обоснования постоянных/временных разведочных кондиций для подсчета запасов полезного ископаемого; составление отчетных материалов по геологическому изучению недр, в том числе с подсчетом запасов или оценкой ресурсов полезного ископаемого;</li>
                    <li>разработка и подготовка исходных геологических данных для проектирования добычи и переработки (обогащения) ископаемых солей и современных соляных месторождений;</li>
                    <li>геологическое сопровождение добычных работ (рудничная геология); геоинформационное сопровождение работ по объектам исследований и проектирования;</li>
                    <li>изучение вещественного состава солей (минералогия, петрография, геохимия) и изменчивости их свойств в пределах месторождения и отдельных его участков;</li>
                    <li>районирование залежей солей в пределах месторождений, их шахтных полей, по геолого-гидрогеологическим параметрам;</li>
                    <li>участие в комплексных исследованиях, направленных на обеспечение безопасной работы калийных рудников с использованием современных методов, включая цифровое моделирование, факторный анализ и т.д.</li>
                    
                </ul>
            </Text> 
            
        </>
    );
}

export default Science__Geology;