import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Culture__Mind = (props) => {

    const images = [
        {
            "url": pathstr+"data/events/engineer/galurgy-1.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'Инженер года, 2016-2017 гг.'
        },
        {
            "url": pathstr+"data/events/engineer/galurgy-2.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'Инженер года, 2016-2017 гг.'
        }
    ];
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="1rem" />
            <Text width="1000px">
                <p>В 2016, 2017г в Институте проводился Конкурс «Инженер года» с целью развития и повышения престижа рационализаторской деятельности среди работников Института, повышения их творческой активности.</p>
                <p>Работники АО «ВНИИ Галургии» принимают участие и во внешних конкурсах. Так в 2020г. работники Института приняли участие в VII ежегодном конкурсе профессионального мастерства «Лучший по профессии в комплексе капитального строительства атомной отрасли» в номинации «Информационное моделирование и проектирование».</p>
            </Text>
            <Gallery images={images} scale="300" color="transparent"  caption />    
            <Spacer space="1rem" /> 
            <Text width="1000px">    
                <p>Команда АО «ВНИИ Галургии» полноценно выполнила все задания конкурса и заняла призовое 2 место. Работники были награждены ценными призами. Судьями Конкурса особо отмечены слаженная командная работа и профессионализм сотрудников АО «ВНИИ Галургии».</p>
                <p>В 2021 года в г. Сочи состоялся Первый Международный строительный чемпионат, на котором команда Института заняла Первое место в командной номинации «Информационное моделирование и проектирование».  Кроме того, наша команда удостоилась специального приза от российского подразделения Building Smart «За лучшее практическое применение знаний в области openBIM подхода».</p>
               
            </Text> 
            
            <ImageFull image={pathstr+"data/awards/buildchamp.jpg"} caption="Команда ВНИИГ — награждение"    />
            
        </>
    );
}

export default Culture__Mind;