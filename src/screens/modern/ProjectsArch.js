import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Projects__Arch = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
           
            <Text width="1000px">
                <p>Комплексная разработка архитектурных и конструктивных решений зданий и сооружений для строительства новых и реконструкции действующих предприятий с применением передовых строительных технологий и прогрессивных материалов, увеличивающих сроки службы строительных конструкций, повышающих эффективность инвестиционных затрат.</p>
                <ul>
                    <h3>Основные направления деятельности:</h3>
                    <li>разработка архитектурно-строительных решений;</li>
                    <li>разработка конструктивных решений;</li>
                    <li>разработка конструктивных решений зданий и сооружений на подрабатываемых и не подрабатываемых горными выработками территориях всех уровней ответственности, включая особо опасные и технически сложные объекты;</li>
                    <li>расчетное обоснование прочности, устойчивости, нормальной и комфортной эксплуатации строительных объектов и их отдельных частей с учетом всех видов воздействий на конструктивные элементы в современных вычислительных программных системах;</li>
                    <li>разработка документации КМ (конструкции металлические), КЖ (конструкции железобетонные) и КД (конструкции деревянные);</li>
                    <li>разработка документации по объектам
                        <ul>
                            <li>производственные здания и сооружения и их комплексы различного технологического назначения I и II уровней ответственности;</li>
                            <li>административно-бытовые здания и сооружения и их комплексы;</li>
                            <li>материально-технического снабжения;</li>
                            <li>объекты межцехового транспорта.</li>
                        </ul>
                    </li>
                    
                </ul>
            </Text> 
            
            
        </>
    );
}

export default Projects__Arch;