import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Science__Auto = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="1rem" />
            <Text width="1000px">

                <ul>
                    <h3>Основные направления деятельности:</h3>
                    <li>разработка автоматизированных систем управления технологическими процессами (АСУТП) в составе проектов новых и реконструкции действующих производств;</li>
                    <li>исследование физических свойств технологических сред с целью подготовки технических решений по контролю технологических параметров;</li>
                    <li>разработка технических предложений по методам и средствам контроля и информационно-измерительным системам (ИИС);</li>
                    <li>разработка алгоритмов управления и расчетов в составе АСУТП и ИИС.</li>
                    
                </ul>
            </Text> 
            
        </>
    );
}

export default Science__Auto;