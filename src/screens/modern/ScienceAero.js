import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Science__Aero = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="1rem" />
            <Text width="1000px">
                <p>Научно-исследовательская лаборатория рудничной аэрологии и газодинамики горных работ является структурным подразделением горно-геологической научной части АО «ВНИИ Галургии».</p>
                <ul>
                    <h3>Основные направления деятельности:</h3>
                    <li>разработка и совершенствование технических и организационных методов проветривания горных выработок, обеспечивающих безопасность и безаварийность при производстве горных работ, научное сопровождение горных работ в сложных горно-геологических условиях;</li>
                    <li>разработка научных основ автоматизации проветривания шахт и подземных сооружений с целью повышения безопасности и снижения энергопотребления на вентиляцию;</li>
                    <li>разработка нормативных документов, экспертиза проектов строительства и реконструкции шахт, вскрытия и подготовки выемочных полей, горизонтов, блоков и панелей, в части вентиляции, проветривания и борьбы с внезапными выбросами.</li>
                    
                    
                </ul>
            </Text> 
            
        </>
    );
}

export default Science__Aero;