import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Science__Plugs = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="1rem" />
            <Text width="1000px">
                <p>Научно-исследовательская лаборатория технологии закладочных работ и тампонажных материалов является структурным подразделением научной части АО «ВНИИ Галургии».</p>
                <ul>
                    <h3>Основные направления деятельности:</h3>
                    <li>проведение исследований и разработка технологических процессов закладки горных выработок и складирования солеотходов;</li>
                    <li>выполнение актуальных научно-исследовательских работ в области разработки, совершенствования и экспертизы составов тампонажных материалов;</li>
                    <li>проведение опытных работ на опытных участках рудников и промышленных испытаний на действующих рудниках;</li>
                    <li>создание методик и разработка экспериментальных установок, проведение экспериментальных исследований, обобщение и оценка полученных данных, оформление отчетов;</li>
                    <li>подготовка исходных данных на проектирование, оказание технического содействия и участие во внедрении результатов НИР.</li>
                    
                    
                </ul>
            </Text> 
            
        </>
    );
}

export default Science__Plugs;