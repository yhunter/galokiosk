import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Culture__Youth = (props) => {

    const images1 = [
        {
            "url": pathstr+"data/events/grow/galurgy-grow-1.jpg",
            "width": 1280,
            "height": 853,
            "title": 'КВН, г. Пермь, 2017 г.'
        },
        {
            "url": pathstr+"data/events/grow/galurgy-grow-2.jpg",
            "width": 1280,
            "height": 914,
            "title": 'КВН, г. Пермь, 2017 г.'
        },
        {
            "url": pathstr+"data/events/grow/galurgy-grow-3.jpg",
            "width": 1280,
            "height": 853,
            "title": 'КВН, г. Пермь, 2017 г.'
        },
        {
            "url": pathstr+"data/events/grow/galurgy-grow-4.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'Интеллектуальные игры, г. Пермь, 2018 г.'
        },
        {
            "url": pathstr+"data/events/grow/galurgy-grow-5.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'Интеллектуальные игры, г. Пермь, 2018 г.'
        },
        {
            "url": pathstr+"data/events/grow/galurgy-grow-6.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'Веревочный курс, г. Пермь, 2015 г.'
        },
        {
            "url": pathstr+"data/events/grow/galurgy-grow-7.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'Веревочный курс, г. Пермь, 2015 г.'
        },
        {
            "url": pathstr+"data/events/grow/galurgy-grow-8.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'Веревочный курс, г. Пермь, 2015 г.'
        }
    ];
    const images2 = [
        {
            "url": pathstr+"data/events/charity/galurgy-charity-1.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'Благотворительная деятельность, г. Пермь, 2018-2019 г.'
        },
        {
            "url": pathstr+"data/events/charity/galurgy-charity-2.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'Благотворительная деятельность, г. Пермь, 2018-2019 г.'
        },
        {
            "url": pathstr+"data/events/charity/galurgy-charity-3.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'Благотворительная деятельность, г. Пермь, 2018-2019 г.'
        },
        {
            "url": pathstr+"data/events/charity/galurgy-charity-4.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'Благотворительная деятельность, г. Пермь, 2018-2019 г.'
        },
        {
            "url": pathstr+"data/events/charity/galurgy-charity-5.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'Благотворительная деятельность, г. Пермь, 2018-2019 г.'
        }
    ];
    const images3 = [
        
        {
            "url": pathstr+"data/events/veterans/galurgy-2.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'Мероприятия для ветеранов, г. Пермь, 2015 г.'
        },
        {
            "url": pathstr+"data/events/veterans/galurgy-3.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'Мероприятия для ветеранов, г. Пермь, 2015 г.'
        },
        {
            "url": pathstr+"data/events/veterans/galurgy-4.jpg",
            "width": 1280,
            "height": 1920,
            "title": 'Мероприятия для ветеранов, г. Пермь, 2015 г.'
        },
        {
            "url": pathstr+"data/events/veterans/galurgy-6.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'Мероприятия для ветеранов, г. Пермь, 2015 г.'
        },
        {
            "url": pathstr+"data/events/veterans/galurgy-7.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'Мероприятия для ветеранов, г. Пермь, 2015 г.'
        },
        {
            "url": pathstr+"data/events/veterans/galurgy-1.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'Мероприятия для ветеранов, г. Пермь, 2015 г.'
        }
        
    ];
    const images4 = [
        
        {
            "url": pathstr+"data/events/culture/galurgy-1.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'День здоровья, г. Пермь, 2016 г.'
        },
        {
            "url": pathstr+"data/events/culture/galurgy-2.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'День здоровья, г. Пермь, 2017 г.'
        },
        {
            "url": pathstr+"data/events/culture/galurgy-3.jpg",
            "width": 1280,
            "height": 1920,
            "title": 'Международный женский день, 2017 г.'
        },{
            "url": pathstr+"data/events/culture/galurgy-4.jpg",
            "width": 1920,
            "height": 1280,
            "title": 'День защитника Отечества, г. Пермь, 2017 г.'
        }
        
    ];
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="1rem" />
            <Text width="1000px">
                <p>
                    С 2015 года в Институте сформирована инициативная группа – Совет Молодежи. Ее основной задачей является вовлечение молодых специалистов в жизнь Института.
                </p>
                <p><b>Члены Совета молодежи активно участвуют в реализации следующих инициатив:</b></p>
                <ul>
                    
                    <li>Личностное развитие молодых работников.</li>
                    <li>Спортивные мероприятия.</li>
                    <li>Благотворительная и волонтерская деятельность.</li>
                    <li>Мероприятия для ветеранов</li>
                    <li>Культурно-массовые мероприятия Института</li>
                    <li>Личностное развитие молодых работников</li>
                    
                </ul>
                <h3>Личностное развитие молодых работников</h3>
                <ul>
                    <p><b>Развитие молодых специалистов происходит путем их вовлечения в командообразующие  мероприятия, такие как:</b></p>
                    <li>«Веревочный курс»</li>
                    <li>Интеллектуальные игры, on-line конкурсы</li>
                    <li>Игры КВН</li>
                    
                    
                </ul>
                </Text>
                <Gallery images={images1} scale="250" color="transparent" caption />    
                <Spacer space="1rem" /> 
                <Text width="1000px">
                <p>Кроме того, проведение подобных мероприятий способствуют сплочению коллектива Института.</p>


                    <h3>Спортивные мероприятия</h3>

                    
                    

                <p>Совет молодежи активно участвует в проведении различных спортивных мероприятий, проходящих в Институте (чемпионат по футболу, баскетболу, настольному теннису, и.т.д).</p>
                <h3>Благотворительная и волонтерская деятельность</h3>
                <p>Регулярно в институте проводятся благотворительные мероприятия. Работники Института и члены Совета молодежи становятся волонтерами, осуществляют сбор и доставку средств и предметов первой необходимости в благотворительные детские фонды и приюты для животных. </p>
                </Text>
                <Gallery images={images2} scale="250" color="transparent" caption />    
                <Spacer space="1rem" /> 
                <Text width="1000px">
                <h3>Мероприятия для ветеранов</h3>
                <p>Не забывает институт и про своих ветеранов. Члены Совета молодёжи ежегодно поздравляют ветеранов праздничными концертами.</p>
                </Text>
                <Gallery images={images3} scale="250" color="transparent" caption />    
                <Spacer space="1rem" /> 
                <Text width="1000px">
                <h3>Культурно-массовые мероприятия Института</h3>
                <p>В течении года для работников всего Института Советом молодежи устраиваются мероприятия, посвященные празднованию календарных праздников и не только… В последние годы особое внимание уделяется укреплению здоровья работников и профилактике заболеваний. Праздничные мероприятия и поздравление работников института проводятся также и в on-line формате.</p>
                </Text>
                <Gallery images={images4} scale="350" color="transparent" caption />    
                  
           
            
            
        </>
    );
}

export default Culture__Youth;