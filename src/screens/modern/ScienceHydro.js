import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Science__Hydro = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="1rem" />
            <Text width="1000px">
                <p>Гидрогеология, гидрология месторождений водорастворимых полезных ископаемых – основная цель исследований, проводимых в научно-исследовательской лаборатории гидрогеологии и охраны природных вод АО «ВНИИ Галургии» (филиал в Санкт-Петербурге).</p>
                <ul>
                    <h3>Основные направления деятельности:</h3>
                    <li>гидрогеология месторождений полезных ископаемых на стадиях разведки, освоения и эксплуатации: изучение особенностей формирования динамического режима и химического состава подземных вод;</li>
                    <li>научно-методическое сопровождение мониторинга рудничных рассолопроявлений;</li>
                    <li>мониторинговые исследования состояния гидросферы;</li>
                    <li>мониторинг безопасности гидротехнических сооружений;</li>
                    <li>мониторинг объектов наземного складирования солеотходов;</li>
                    <li>разработка водоохранных мероприятий;</li>
                    <li>мониторинг состояния месторождений гидроминерального сырья и технологии переработки.</li>
                    
                </ul>
            </Text>
            
        </>
    );
}

export default Science__Hydro;