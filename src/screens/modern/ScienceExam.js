import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Science__Exam = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="1rem" />
            <Text width="1000px">
                <p>Обеспечение качества готовой продукции в соответствии с требованиями мирового и внутреннего рынков — основная задача научно-исследовательской лаборатории испытаний и стандартизации продукции АО «ВНИИ Галургии» (филиал в Санкт-Петербурге).</p>
                <ul>
                    <h3>Основные направления деятельности:</h3>
                    <li>изучение физико-механических свойств удобрений и солей;</li>
                    <li>устранение пылимости и слеживаемости сыпучих материалов;</li>
                    <li>повышение прочности гранулированных удобрений;</li>
                    <li>поиск новых реагентов для обработки калийных удобрений с улучшением их физико-механических потребительских свойств;</li>
                    <li>осуществление контроля качества экспортной калийной продукции в портах отгрузки.</li>
                    
                </ul>
            </Text> 
            
        </>
    );
}

export default Science__Exam;