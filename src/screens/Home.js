//Главная страница с выбором разделов
import AppBar from '../components/AppBar'; //Верхняя панелька приложения
import MainMenu from '../components/MainMenu'; //Главное меню
import Timer from '../components/Timer';

function Home() {
    const header = [//Заголовок главной страницы для передачи в AppBar 
        {
            title: "Интерактивный музей: список разделов",
            route: "/"
        }
    ];
    return(
        <div className="App">
            <section className="home">
                <AppBar menuButton="hideButton" backButton="hideButton" header={header} hideHome /> 
                <header>
                    <div className="logo">
                        <img src="/img/logo.png" alt="ВНИИ Галургии" />
                        
                    </div>
                    <div className="subtitle">
                        История. Технологии. Люди
                    </div>                    
                </header>
                <MainMenu /> {/* Главное меню */}
                
                <div className="rightShadow">

                </div>
                <Timer />
            </section>
            
        </div>
    );
}

export default Home;