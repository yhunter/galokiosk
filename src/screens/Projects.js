//Страница персоналии
import AppBar from '../components/AppBar';
import Shadow from '../components/Shadow';
import MainMenu from '../components/MainMenu';
import Timer from '../components/Timer';
import React, { useState } from 'react';
import ScrollContainer from 'react-indiana-drag-scroll';

import {projects} from '../constants/projects.js';
import {technologiesChronology} from '../constants/techChronology.js';
import {expeditionsChronology} from '../constants/expChronology.js';
import {history} from '../constants/history.js';


const Projects = (props) => {
    const [menu, setMenu] = useState('hideMenu'); //Состояние меню
    const showMenu = () => { //Переключатель появления меню
        let menuStatus = (menu === "hideMenu") ? "showMenu" : "hideMenu";
        setMenu(menuStatus);
    }
    const [person, setPerson] = useState(0); //Состояние выбора персоны

    const selectPerson = (item) => { //Функция выбора персоны
        setPerson(item);
    }
    //console.log(persons);
    const header = [
        
        props.tree[props.cat],
        props.tree[props.cat].items[props.subpage]

    ];

    let chronoArr = projects;

    if (props.cat==="ch1") {
        if (props.subpage===9) chronoArr=history;
    }
    if (props.cat==="ch3") {
        if (props.subpage===6) chronoArr=technologiesChronology;
        if (props.subpage===7) chronoArr=expeditionsChronology;
    }
    

    
    return(
        <div className="App">
            <section className={"page projects "+menu}>
                <AppBar header={header} menuCallBack={showMenu}  />
                
                <div   className="projects__field" >
                    {chronoArr.map((item, index) => (
                            <section data-index={index} key={index} className={index === person ? 'active' : ''}>                                    
                                <article>
                                    <div className="projects__period">
                                        <div className="projects__date"><span>{item.date}</span></div>
                                        <div className="projects__year">{item.year}</div>
                                    </div>
                                    <div className="projects__container">
                                        <div className="projects__text"  dangerouslySetInnerHTML={{__html: item.text}}></div>

                                    </div>                                      
                                </article>
                            </section>
                    ))}              
                    
                    
                </div>
                
                
                <div className="rightShadow">

                </div>
                <Shadow />
                <MainMenu />
                <Timer />
            </section>
        </div>
    );
}

export default Projects;