//Страница персоналии
import AppBar from '../components/AppBar';
import Shadow from '../components/Shadow';
import MainMenu from '../components/MainMenu';
import Timer from '../components/Timer';
import React, { useState, useRef } from 'react';
import ScrollContainer from 'react-indiana-drag-scroll';

import {persons} from '../constants/persons.js';
import {candidates} from '../constants/candidates';
import {pathstr} from '../constants/tree';

const Persons = (props) => {
    let personsData = persons; let personsDir = 'persons'; // Константы для персоналий
    if (props.cat === 'ch6') { //Константы для персоналий кандидатов наук
        personsData = candidates;
        personsDir = 'candidates';
    }
    const [menu, setMenu] = useState('hideMenu'); //Состояние меню
    const showMenu = () => { //Переключатель появления меню
        let menuStatus = (menu === "hideMenu") ? "showMenu" : "hideMenu";
        setMenu(menuStatus);
    }
    const [person, setPerson] = useState(0); //Состояние выбора персоны

    const selectPerson = (item) => { //Функция выбора персоны
        setPerson(item);
    }
    //console.log(persons);
    const header = [
        
        props.tree[props.cat],
        props.tree[props.cat].items[props.subpage]

    ];

    const imagesPath = pathstr+'data/'+personsDir+'/'; //Путь до фотографий

    const personImage = (img, alt) => { //Если есть адрес изображения, выводим. Если нет - null
        if (img!=="") {
            return (
                <>
                    <img className="person__thumb" alt={alt} src={imagesPath+img} />
                </>
            );
        }
        else return null;
        
    }

    const container = useRef(null);

    const scrollToRef = () => {
        container.current.scrollTo(0, 0);
    }
    
    return(
        <div className="App">
            <section className={"page person "+menu}>
                <AppBar header={header} menuCallBack={showMenu}  />
                
                <ScrollContainer   className="person__field"  innerRef={container} >
                {personsData[props.subpage][person] !== undefined && //Если массив персоналий этого раздела не пустой, то выводим первую карточку
                    <div className="person__container">
                        <article className="person__item">
                            <h2>{personsData[props.subpage][person].name}</h2>
                            <div className="person__text">
                               
                                {personImage(personsData[props.subpage][person].photo, personsData[props.subpage][person].name)}
                                <p dangerouslySetInnerHTML={{__html: personsData[props.subpage][person].text}}></p>                            
                            </div>
                        </article>
                    </div>
                }                  
                    
                    
                </ScrollContainer>
                <div className="person__select">
                    <ScrollContainer  className="scroll-container">
                    <ul>
                        {personsData[props.subpage].map((item, index) => (
                            <li data-index={index} key={index} className={index === person ? 'active' : ''}>                                    
                                <button onClick={(element) => {scrollToRef(); selectPerson(index); } }> 
                                    {item.name}   
                                </button>
                            </li>
                        ))}
                    </ul>
                    </ScrollContainer>
                </div>
                
                <div className="rightShadow">

                </div>
                <Shadow />
                <MainMenu />
                <Timer />
            </section>
        </div>
    );
}

export default Persons;