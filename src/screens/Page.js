//Страница текстового содержимого


import AppBar from '../components/AppBar';
import Shadow from '../components/Shadow';
import MainMenu from '../components/MainMenu';

import React, { useState } from 'react';


import Gallery from '../components/Gallery';
import Image from '../components/Image';
import Timer from '../components/Timer';


import Text from '../components/TextBlock';
import Spacer from '../components/Spacer';
import ImageFull from '../components/ImageFull';
import SlideShow from '../components/SlideShow';

import ReactPlayer from 'react-player';

import "@fontsource/nunito";


import Page__Test from "./pages/Test";






function Page(props) {

    

    


    const pageSelector = () => { //Если в pageTree есть нужный компонент, то выводим его. Если нет, то тестовую страницу (потом 404)
        if (props.tree[props.cat].items[props.subpage].component) {
            return(props.tree[props.cat].items[props.subpage].component);
        }
        else {
            return(<Page__Test />); 
        }

    }
    
    

    const [menu, setMenu] = useState('hideMenu');
    const showMenu = () => {
        let menuStatus = (menu === "hideMenu") ? "showMenu" : "hideMenu";
        setMenu(menuStatus);
    }
    console.log(props);
    const header = [
        
        props.tree[props.cat],
        props.tree[props.cat].items[props.subpage]

    ];
    return(
        <div className="App">
            <section className={"page textfild "+menu}>
                <AppBar header={header} menuCallBack={showMenu}  />
                    
                    {pageSelector()}
                      
                
                <div className="rightShadow">

                </div>
                <Shadow />
                <MainMenu />
                <Timer />
            </section>
        </div>
    );
}

export default Page;