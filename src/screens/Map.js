//Страница карты проектов на глобусе 

import AppBar from '../components/AppBar'; //Верхняя панелька приложения
import Shadow from '../components/Shadow'; //Затемнение фона для меню
import MainMenu from '../components/MainMenu'; //Главное меню
import Timer from '../components/Timer';
import React, { useState } from 'react';

import Earth from '../components/Globe'; //Компонент для отображения планеты


import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';




function Map(props) {
    const [dialog, setDialog] = useState({'status': 'hideDialog'});
    const showDialog = (item, content) => { //Колбек для показа модального окна с описанием объекта на карте
        setDialog({'status': 'showDialog', 'item': item, 'content': content});
    }

    const hideDialog = () => {
        setDialog({'status': 'hideDialog'});
    }
    
    //console.log(props);


    

    


    
    const [menu, setMenu] = useState('hideMenu');
    const showMenu = () => {
        let menuStatus = (menu === "hideMenu") ? "showMenu" : "hideMenu";
        setMenu(menuStatus);
    }

    
    

    const header = [
        
        props.tree[props.cat],
        props.tree[props.cat].items[props.subpage]

    ];

    //console.log(header);

    
    return(
        <div className="App map">
            <div className="loader">
                <div class="spinner">
                    <div class="circle outer"></div>
                    <div class="circle inner"></div>
                    <div class="circle main"></div>
                </div>
            </div>
            <section  className={"subselector  page "+menu}>
                        <AppBar header={header}   menuCallBack={showMenu}  />
                        <div className="globe">
                            <Earth callBack={showDialog} />
                            <div className={"globe__dialog "+dialog.status}>  
                                
                                <IconButton aria-label="delete" className="globe__dialog__close" onClick={() => hideDialog()}><CloseIcon /></IconButton>
                                <div className="globe__dialog__content dialog">
                                    <div className="dialog__header">
                                        {dialog.item}
                                    </div>
                                    <div className="dialog__content" dangerouslySetInnerHTML={{__html: dialog.content}}>
                                        
                                    </div>                                  
                                    
                                </div>
                                
                            </div>
                            <div className="globe__legend legend">
                                <div className="legend__projects">
                                    Проекты
                                </div>
                                <div className="legend__partners">
                                    Партнеры
                                </div>
                            </div>
                        </div>                 
                
                        <div className="rightShadow">

                        </div>
                        <Shadow />
                        <MainMenu menuCallBack={showMenu}  />
                        <Timer  />
            </section>
        </div>
    );
}

export default Map;