//Страница выбора подраздела
import AppBar from '../components/AppBar';
import Shadow from '../components/Shadow';
import Timer from '../components/Timer';
import MainMenu from '../components/MainMenu';
import { Link } from 'react-router-dom';
import React, { useState } from 'react';

import handleViewport from 'react-in-viewport'; //Отслеживание элементов во вьюпорте





function Submenu(props) {
    //console.log(props);
    const category = props.cat;
    
    const tree = props.tree;

    //console.log(category);

    

    const Block = (props: { inViewport: boolean }) => {
        const { inViewport, forwardedRef } = props;

        const subMenuClass = inViewport ? 'showSubMenu' : 'hideSubMenu';
        
        return (

            <div className={"submenu "+subMenuClass}>
                        

                <ul className="viewport-block" ref={forwardedRef}>
                
                    {tree[category].items.map((item, index) => (
                        <li data-index={index} key={index}>                                    
                            <Link to={item.route}>
                                <div className="submenu__counter">
                                    {index+1}
                                </div>
                                <div className="submenu__title" dangerouslySetInnerHTML={{__html: item.title}} >
                                    
                                    
                                </div>    
                            </Link>
                        </li>
                    ))}
                    

                </ul>
            </div>
        );
      };
      
    const ViewportBlock = handleViewport(Block, /** options: {}, config: {} **/);


    
    const [menu, setMenu] = useState('hideMenu');
    const showMenu = () => {
        let menuStatus = (menu === "hideMenu") ? "showMenu" : "hideMenu";
        setMenu(menuStatus);
    }

    
    

    const header = [        
        tree[category]
    ];

    

    
    return(
        <div className="App sub">
            <section  className={"subselector page "+menu}>
                <AppBar header={header}   menuRoot  menuCallBack={showMenu}  />
                <div className="textField">
                    
                    <ViewportBlock>
                        &nbsp;
                    </ViewportBlock>
                </div>
                
                <div className="rightShadow">

                </div>
                <Shadow />
                <Timer />
                
            </section>
        </div>
    );
}

export default Submenu;