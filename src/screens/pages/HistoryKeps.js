import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Page__HistoryKeps = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="3rem" />
            <Text width="1000px">
                <ul>
                    <li><p>21 января 1915 г. академик В.И. Вернадский огласил на заседании Физико-математического отделения Академии наук заявление, подготовленное группой ученых, в которую входили: Н.И. Андрусов, Б.Б.Голицын, А.П. Карпинский и Н.С. Курнаков. В заявлении было выдвинуто предложение создать в системе Академии наук постоянную комиссию для исследования естественных производительных сил страны.</p>
                    <figure>
                        <img src={pathstr+"data/history/kuludinskie.jpg"} alt="Фрагмент карты Томской губернии, конец XIX века. Кулундинские соленые озера" crossorigin="anonymous" />
                        <figcaption>Фрагмент карты Томской губернии, конец XIX века. Кулундинские соленые озера</figcaption>
                    </figure>
                    <p>4 февраля 1915 г Комиссия по изучению естественных производительных сил России (КЕПС) была создана. В совет Комиссии вошли академики: Б.Б. Голицын, Н.С. Курнаков, А.С. Фаминицын, П.И. Вальден, Ф.Ю. Левинсон-Лессинг и др. В программе деятельности КЕПС было записано: «Издание Сводного труда по описанию известных природных богатств страны и исследование новых природных объектов, недостаточно изученных для использования в промышленности, в том числе соляных месторождений».</p></li>
                    <li>
                        <p>Весной 1915 г. в составе КЕПС начала работу Подкомиссия по солям.</p>
<p>Основные направления деятельности Подкомиссии по солям были определены в докладе Н.С. Курнакова «Задачи химического исследования русских соляных озер».</p>
                    </li>
                    <li>10 января 1917 г. на совместном заседании КЕПС и Военно-химического комитета инициатива создания сети исследовательских институтов, и прежде всего организация институтов в области химических наук, была поддержана.</li>
                    <li>В 1917 г. Подкомиссию по солям была преобразована в Соляной отдел КЕПС под руководством академика Н.С. Курнакова. В состав отдела вошли также академики А.П. Карпинский, К.Ф. Белоглазов, Н.Н. Ефремов, В.П. Ильинский, Н.И. Подкопаев, Е.Е. Уразов и др.</li>
                    <li>В 30-е гг. в ходе коренной перестройки Комиссию по изучению естественных производительных сил Академии наук России упразднили. На ее основе были созданы 16 самостоятельных научно-исследовательских института. В том числе и Соляная лаборатория АН СССР (СОЛАБ).</li>
                </ul>
                

               
            </Text>
            <Spacer space="3rem" />
            
        </>
    );
}

export default Page__HistoryKeps;