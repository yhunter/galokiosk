import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Page__ModernManagement = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="3rem" />
            <Text width="1000px">
                <ul>
                    <li>Шкуратский Д. Н. – Генеральный директор</li>
                    <li>Ванк В. В. – Технический директор</li>
                    <li>Мясоедов Н. В. – Директор проектной части, г. Санкт-Петербург</li>
                    <li>Скопинов М. В. – Директор проектной части, г. Пермь</li>
                    <li>Титков С. Н. – Директор технологической научной части</li>
                    <li>Кудасов Д. Н. – Директор по ИТ</li>
                    <li>Кириллов Р. Н. – Директор по развитию</li>
                    <li>Погорелова Ю. А. – Директор по экономике и финансам</li>
                    <li>Быстрых Е. К. – Директор по персоналу</li>
                    <li>Поповичев Д. В. - Заместитель директора проектной части, г. Санкт-Петербург</li>
                    <li>Клемятова О. А. – Заместитель директора по развитию</li>
                    <li>Дьяконов М. В. – Заместитель Технического директора</li>
                    <li>Коробейников А. А. – Начальник технического управления</li>
                    <li>Месяц Е. А.  – Начальник управления безопасности</li>
                    <li>Нургалин Х.Х. – Начальник административно-хозяйственного управления</li>
                    <li>Ханжин Е. А. – Начальник управления строительного контроля</li>
                </ul>
               
            </Text>
            
        </>
    );
}

export default Page__ModernManagement;