import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Page__HistoryReunion = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="3rem" />
            <Text width="1000px">
                <figure>
                        <img src={pathstr+"data/history/spb.jpg"} alt="Здание АО «ВНИИ Галургии», г. Санкт-Петербург" crossorigin="anonymous" />
                        <figcaption>Здание АО «ВНИИ Галургии», г. Санкт-Петербург</figcaption>
                </figure>
                <p>В 2016 году произошло воссоединение двух институтов – ОАО «Галургия» и АО «ВНИИ Галургии». Объединенный институт АО «ВНИИ Галургии» располагается на трех площадках — в городах Санкт-Петербург, Пермь и Березники.</p>
               
            </Text>
            <ImageFull image={pathstr+"data/history/perm.jpg"} caption="Здание АО «ВНИИ Галургии», г. Пермь"  />
        </>
    );
}

export default Page__HistoryReunion;