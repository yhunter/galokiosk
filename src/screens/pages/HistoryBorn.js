import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Page__HistoryKeps = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;

    const images1 = [
        {
            "url": pathstr+"data/history/galurgy-015.jpg",
            "width": 1135,
            "height": 1920,
            "title": 'Приказ о создании ВНИИГ 1971 г.'
        },
        {
            "url": pathstr+"data/history/galurgy-017.jpg",
            "width": 1133,
            "height": 1920,
            "title": 'Приказ о создании ВНИИГ 1971 г.'
        },
        {
            "url": pathstr+"data/history/galurgy-016.jpg",
            "width": 1161,
            "height": 1920,
            "title": 'Приказ о создании ВНИИГ 1971 г.'
        }
    ];
    const images2 = [
        {
            "url": pathstr+"data/history/galurgy-order-000.jpg",
            "width": 1124,
            "height": 1403,
            "title": 'Приказ о создании уральского филиала 1972 г.'
        },
        {
            "url": pathstr+"data/history/galurgy-order-001.jpg",
            "width": 1024,
            "height": 1555,
            "title": 'Приказ о создании уральского филиала 1972 г.'
        },
        {
            "url": pathstr+"data/history/galurgy-order-002.jpg",
            "width": 1024,
            "height": 1559,
            "title": 'Приказ о создании уральского филиала 1972 г.'
        },
        {
            "url": pathstr+"data/history/galurgy-order-003.jpg",
            "width": 1024,
            "height": 1579,
            "title": 'Приказ о создании уральского филиала 1972 г.'
        },
        {
            "url": pathstr+"data/history/galurgy-order-004.jpg",
            "width": 1024,
            "height": 1389,
            "title": 'Приказ о создании уральского филиала 1972 г.'
        }
    ];
    return (
        <>
            <Spacer space="3rem" />
            <Text width="1000px">
                <p>1971 г. На базе научно-исследовательских лабораторий ВНИИГа были созданы Березниковский, Белорусский и Калушский филиалы  ВНИИГа. В головном институте были реорганизованы или вновь созданы группы лаборатории НТИ: редакционно-издательская, патентно-лицензионной и изобретательской работы, информации и СИФа.</p>
                <p>1971 г. На базе Калушской и Стебниковской лаборатории был создан Калушский научно-исследовательский филиал ВНИИГа.</p>
                <p>1971 г. Образован  Белорусский филиал Всесоюзного научно-исследовательского института галургии (БФ ВНИИГ).</p>
            </Text>    
            <Spacer space="1rem" />
            <Gallery images={images1} scale="1000" caption color="transparent" /> 
            <Spacer space="2rem" />
            <Text width="1000px">    
                <p>1971 г. БНИЛ была преобразована в Березниковский филиал ВНИИГ. В составе Березниковского филиала работали 5 секторов, 4 группы.</p>
                <p>1972 г. Приказом МХП Березниковский филиал преобразован в Уральский филиал ВНИИГа в г. Перми. Березниковская лаборатория была включена в состав Уральского филиала. В головном институте (г. Ленинград) были реорганизованы или вновь созданы лаборатория физико- химических исследований процессов флотации и реагентов, группа лаборатории НТИ анализа и обобщения зарубежного опыта.</p>
            </Text>    
            <Spacer space="1rem" />
            <Gallery images={images2} scale="450" caption  color="transparent" /> 
            <Spacer space="2rem" />
            <Text width="1000px">     
                <p>1973 г.  Реорганизованы или вновь созданы: отдел координации научно-исследовательских и опытных работ, математического моделирования с вычислительным центром, группа лаборатории НТИ - бюро по изобретательству и рационализации.</p>
                <figure>
                        <img src={pathstr+"data/history/galurgy-perm.jpg"} alt="ВНИИГ Галургии на площади Карла Маркса. г. Пермь" crossorigin="anonymous" />
                        <figcaption>ВНИИГ Галургии на площади Карла Маркса. г. Пермь</figcaption>
                </figure>
                <p>1973 г. Сформирована проектная и научная части института Уральского Филиала ВНИИ Галургии.</p>
                <p>1974 г. Реорганизована лаборатория стандартизации и методов аналитического контроля.</p>
                <p>1976 г. Создан горно-механический отдел Уральского филиала ВНИИГ.</p>
                <p>1984 г. В Перми организована лаборатория технологии закладочных работ.</p>
                
               
            </Text>
            <Spacer space="2rem" />
            
        </>
    );
}

export default Page__HistoryKeps;