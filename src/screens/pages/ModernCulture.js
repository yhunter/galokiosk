import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Page__ModernCulture = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="3rem" />
            <Text width="1000px">
                <p>Корпоративная культура института является отражением кадровой политики и основывается на ценностях, продекларированных ПАО «Уралкалий», в то же время отражающих специфику института:</p>
                <h3>Мы лидеры</h3>
                <ul>
                    <li>Мы уверенно берем на себя ответственность.</li>
                </ul>
               
            </Text>
            
        </>
    );
}

export default Page__ModernCulture;