import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Page__Floatation = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="3rem" />
            <Text width="1000px">
                <p>Флотационный метод обогащения калийных руд, разработанный научными работниками ВНИИГ, получил широкое распространение на горно-обогатительных
калийных предприятиях на всей территории СССР и стал использоваться по всему
миру.</p></Text>
            <Spacer space="1rem" />
            <ImageFull image={pathstr+"data/floatation.png"} caption="Блок-схема производства удобрений на ПАО «Уралкалий»" noshadow  />
            <Spacer space="1rem" />
            <Text width="1000px">
                <p>Флотация (фр. flottation, англ. flotation) — один из основных методов обогащения полезных ископаемых — процесс разделения минеральных частиц, основанный на избирательном изменении смачиваемости поверхности после обработки реагентом
собирателем, закреплении флотируемых минеральных частиц на пузырьках воздуха
и извлечении их в пенный продукт флотационной машины.</p>

            </Text>
            <Spacer space="3rem" />
            
        </>
    );
}

export default Page__Floatation;