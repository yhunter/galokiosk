import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Directions__Monitoring = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="3rem" />
            <Text width="1000px">
                <p>Институт осуществляет мониторинг и обследование технического состояния строительных конструкций, разрабатывает мероприятия по обеспечению надежности строительных конструкций</p>
                <p><h3>Основные направления работы отдела:</h3>
                    <ul>
                        <li>Мониторинг технического состояния строительных конструкций зданий и сооружений.</li>
                        <li>Детальные инженерные обследования (выявление дефектов и повреждений) и оценка технического состояния строительных конструкций зданий и сооружений с рекомендациями по дальнейшей эксплуатации.</li>
                        <li>Разработка рабочей документации по восстановлению несущей способности строительных конструкций.</li>
                        <li>Экспертная оценка технического состояния строительных конструкций зданий и сооружений.</li>
                    </ul>
                </p>
            </Text>
            
        </>
    );
}

export default Directions__Monitoring;