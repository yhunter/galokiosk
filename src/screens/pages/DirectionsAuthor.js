import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Directions__Author = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Spacer space="3rem" />
            <Text width="1000px">
                <p>Авторский надзор осуществляется на основании договора в соответствии с СП　246.1325800.2016 «Положение об авторском надзоре за строительством зданий и сооружений», СП 11-110-99 «Авторский надзор за строительством зданий и сооружений».</p>
                <p>Институт принимает активное участие в работе подкомитета, комитета по технологическому проектированию объектов производственного назначения и транспортной инфраструктуры Национального объединения изыскателей и проектировщиков по вопросам авторского надзора.</p>
            </Text>
            
        </>
    );
}

export default Directions__Author;