import Gallery from '../../components/Gallery';
import Image from '../../components/Image';
import Text from '../../components/TextBlock';
import Spacer from '../../components/Spacer';
import ImageFull from '../../components/ImageFull';

import {pathstr} from '../../constants/tree';

const Page__Chronology = (props) => {
    //let path = require("path"); 
    //const pathstr = 'file://'+path.dirname(process.execPath).split(props.tree.folder)[0]+props.tree.folder+'/'; 
    //const pathstr = props.path;
    return (
        <>
            <Text width="1000px">
                <p>Галургический метод производства хлористого калия из сильвинитовых руд, называемый также «химическим», известен более 100 лет. Метод, несмотря на название, не связан с химическими превращениями компонентов, а базируется на различной растворимости основных солевых компонентов руд: хлорида калия и хлорида натрия, в зависимости от температуры.</p>
                <p>Галургический метод представляет из себя растворение руды при высоких температурах с выщелачиванием полезных компонентов и последующей вакуум-кристаллизацией при охлаждении</p>
                <p>Отличие галургического хлористого калия от флотационного в том, что эта технология позволяет получать продукт с 99-процентным содержанием основного вещества. Флотационный способ дает только 95-процентный хлористый калий. </p>
            </Text>
            <ImageFull image={pathstr+"data/galurgiya.png"} caption="Галургия"  />
        </>
    );
}

export default Page__Chronology;