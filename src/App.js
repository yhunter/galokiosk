import { BrowserRouter, Route, Switch, useLocation } from "react-router-dom";

import React, { useEffect } from 'react';

import "@fontsource/nunito-sans";
import './scss/style.scss';


import Home from './screens/Home'; //Лендинг
import Submenu from './screens/Submenu'; //Страница
import Page from './screens/Page'; //Страница
import Map from './screens/Map'; //Карта проектов
import Persons from './screens/Persons'; //Персоналии
import Projects from './screens/Projects'; //Проекты
import Modern from './screens/Modern'; //Современность

import SlideShow from './components/SlideShow';
import Timer from './components/Timer';

import {tree, pathstr} from './constants/tree'; //Структура разделов



function App() {
  
  const slideShowProps = { //Настройки слайд-шоу на фоне
      duration: 15000,
      arrows: false
  }
  const slideShowImages = [  //Изображения слайд-шоу на фоне
    pathstr+"data/slides/bg1.jpg",
    pathstr+"data/slides/bg2.jpg",
    pathstr+"data/slides/bg3.jpg",
    pathstr+"data/slides/bg4.jpg",
    pathstr+"data/slides/bg5.jpg",
    pathstr+"data/slides/bg6.jpg",
    pathstr+"data/slides/bg7.jpg",
    pathstr+"data/slides/bg8.jpg",
  ]

 
  
  //console.log(tree);




  return (
    
      <BrowserRouter basename=".">
            
            <SlideShow properties={slideShowProps} slides={slideShowImages} />
            <div className="App viewport"> 
              <Switch>       
                  <Route path='/' component={Home} exact /> {/* Домашняя */}   
                  <Route path='/submenu/:category'  component={(props) => <Submenu tree={tree} cat={props.match.params.category} />}  exact /> {/* Подменю */}  
                  <Route path='/page/:category/map' component={(props) => <Map tree={tree} cat={props.match.params.category} subpage={1} />} exact /> {/* Карта */}  
                  <Route path='/page/:category/persons/:subpage' component={(props) => <Persons tree={tree} cat={props.match.params.category} subpage={props.match.params.subpage} />} exact /> {/* Персоны */}
                  <Route path='/page/:category/candidates/:subpage' component={(props) => <Persons tree={tree} cat={props.match.params.category} subpage={props.match.params.subpage} />} exact /> {/* Персоны кандидатов наук */}
                  <Route path='/page/:category/modern/:subpage' component={(props) => <Modern tree={tree} cat={props.match.params.category} subpage={props.match.params.subpage} />} exact /> {/* Современность */}  
                  <Route path='/page/:category/projects' component={(props) => <Projects tree={tree} cat={props.match.params.category} subpage={0} />} exact /> {/* Проекты */} 
                  <Route path='/page/ch1/chronology' component={(props) => <Projects tree={tree} cat={"ch1"} subpage={9} />} exact /> {/* История - Летопись */}  
                  <Route path='/page/ch3/chronology' component={(props) => <Projects tree={tree} cat={"ch3"} subpage={6} />} exact /> {/* Хронология технологий */} 
                  <Route path='/page/ch3/expeditions' component={(props) => <Projects tree={tree} cat={"ch3"} subpage={7} />} exact /> {/* Хронология экспедиций */} 
                  <Route path='/page/:category/:subpage' component={(props) => <Page tree={tree} cat={props.match.params.category} subpage={props.match.params.subpage} />} exact /> {/* Страница */}     
                  
                  <Route component={Home}/>    
              </Switch>       
              
            </div>
      </BrowserRouter>
    
  );
}

export default App;
