const Quote = (props) => {
    return(
        <blockquote>
            {props.children}
            <cite>{props.author}</cite>
        </blockquote>
    );
}

export default Quote;