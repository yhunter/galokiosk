

const ImageFull = ({image, caption, noshadow}) => { //boolean noshadow - если true, то выводимподпись под картинкой, а не поверх на затемнении
    let noShadowClass = 'shadowClass';
    if (noshadow) noShadowClass='underCaption';
    return(
        <figure className="fullimage">
            <img src={image} alt={caption} />
            <caption className={noShadowClass}>{caption}</caption>
        </figure>
    );
}

export default ImageFull;