function TextBlock(props) {
    //const _width = inViewport ? 'showSubMenu' : 'hideSubMenu';
    return(
        <div className="text" style={{"width": props.width}}> 
            {props.children}
        </div>
    );
    
}
export default TextBlock;