//Карточка раздела в главном меню

import { Link } from 'react-router-dom';

const MenuCard = (props) => {

    return(
        <div className="menu__cell">
            <div className="menu__cell__card">
                
                <Link to={props.route} className="menu__cell__link">
                    <div className="menu__cell__image">
                        <img src={props.image} alt={props.title} />
                    </div>
                    <div className="menu__cell__counter">
                        {props.number}
                    </div>
                    <div className="menu__cell__title">
                        {props.title}
                    </div>
                    
                    
                </Link>
                {/*
                <ul className="menu__cell__submenu">
                        <li><Link to={props.route}>От Комисии по естественным природным силам России к созданию СОляной лаборатории (1918-1931)</Link></li>
                        <li><Link to={props.route}>От Комисии по естественным природным силам России к созданию СОляной лаборатории (1918-1931)</Link></li>
                        <li><Link to={props.route}>От Комисии по естественным природным силам России к созданию СОляной лаборатории (1918-1931)</Link></li>
                        <li><Link to={props.route}>От Комисии по естественным природным силам России к созданию СОляной лаборатории (1918-1931)</Link></li>
                        <li><Link to={props.route}>От Комисии по естественным природным силам России к созданию СОляной лаборатории (1918-1931)</Link></li>
                        <li><Link to={props.route}>От Комисии по естественным природным силам России к созданию СОляной лаборатории (1918-1931)</Link></li>
                        <li><Link to={props.route}>От Комисии по естественным природным силам России к созданию СОляной лаборатории (1918-1931)</Link></li>
                        <li><Link to={props.route}>От Комисии по естественным природным силам России к созданию СОляной лаборатории (1918-1931)</Link></li>
                        <li><Link to={props.route}>От Комисии по естественным природным силам России к созданию СОляной лаборатории (1918-1931)</Link></li>
                        <li><Link to={props.route}>От Комисии по естественным природным силам России к созданию СОляной лаборатории (1918-1931)</Link></li>
                </ul>*/} 
                
                
            </div>
        </div>
    );
}

export default MenuCard;