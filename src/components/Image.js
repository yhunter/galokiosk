//Компонент галереи

import 'photoswipe/dist/photoswipe.css';
import 'photoswipe/dist/default-skin/default-skin.css';


import { Gallery, Item} from 'react-photoswipe-gallery';




 //Подбираем масштаб

function Image(props) { //string props.url - адрес изображения, float props.width - ширина, float props.height - высота, string props.title - название, boolean props.caption //показывать ли описание картинки поверх миниатюры
  
  let thumb = props.url;
  if (props.thumb!== undefined) { thumb = props.thumb; }


    return (
  
    <div className="image">
         <Gallery options={{"shareEl": false, "fullscreenEl": false}} > 

            <Item
                original={props.url}
                thumbnail={props.url}
                width={props.width}
                height={props.height}
                title={props.title}
            >
                {({ ref, open, url=props.url} ) => (
                    <figure>
                        <img alt={props.title} ref={ref} onClick={open} src={thumb} />
                        <caption>{props.caption ? props.title : ''}</caption>
                    </figure>
                )}
                
            </Item>        
        
        </Gallery>
    
    </div>
    );
}

export default Image;