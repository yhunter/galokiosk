import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';


import Slide from '@material-ui/core/Slide';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import HomeIcon from '@material-ui/icons/Home';

import {
    Link,
    useHistory,
  } from "react-router-dom";

function HideOnScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({ target: window ? window() : undefined });
  
  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

HideOnScroll.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

const Header = (props) => {//Выводим либо только заголовок (для главной), либо хлебные крошки для внутренних разделов + кнопки назад, меню
    let history = useHistory();
    let header = props.header;
    //console.log(props.header); 
    if (props.hideHome===undefined) {
        
        return(//Вывод хлебных крошек для подразделов
            <>
                <IconButton aria-label="delete" className="homeButton" onClick={() => history.push("/")}><HomeIcon /></IconButton>
                {header.map((item, index) => (
                    <Link key={index} to={item.route} className="appBar__link">{item.title.replace(/(<([^>]+)>)/gi, "")}</Link> //чистим от тэгов и выводим название раздела
                ))}                
            </>
        );
    }
    else {
        
        return(//Вывод только заголовка для главной
            <>
                {header[0].title}
            </>
        );
    }
    
}

export default function HideAppBar(props) {
    let menuButtonClass = "";
    let backButtonClass = "";
     
    if (props.menuButton) {//Если есть menuButton, то срабатывает маршрутизация на главную. Иначе открываем главное меню поверх страницы
      menuButtonClass=props.menuButton;
    }
    if (props.backButton) {
        backButtonClass=props.backButton;

    }
    let history = useHistory();
    return (
        <>
        <CssBaseline />
        <HideOnScroll {...props}>
            <AppBar className="appBar">
                <div className={"appBar__button left "+backButtonClass}>
                    <IconButton aria-label="delete" className="menuButton" onClick={() => {history.goBack();}}>
                        <ArrowBackIcon />
                    </IconButton>
                </div>
                <div className={"appBar__button right "+menuButtonClass}>
                    {/* Если есть параметр menuRoot, то при клике на кнопку меню - переходним на главную страницу. Если нет, то открываем меню поверх страницы */}
                    <IconButton aria-label="delete" className="menuButton" onClick={() => {if (props.menuRoot) {history.push("/");} else props.menuCallBack();}}> 
                        <MenuIcon />
                    </IconButton>
                </div>
                <Toolbar>
                    <Typography variant="h6" className="appbar__header"><Header header={props.header} hideHome={props.hideHome} /></Typography>
                </Toolbar>

            </AppBar>
        </HideOnScroll>
        <Toolbar />
      
        </>
    );
}