//Компонент главного меню с выбором раздела

import ScrollContainer from 'react-indiana-drag-scroll';

import ArrowBack from '@material-ui/icons/ArrowBack';


import MenuCard from './MenuCard';  

function MainMenu(props) {

    
    return(
        <nav className="menu">
            <ScrollContainer className="scroll-container">
              <div className="menu__container">  
                  <ArrowBack className="arrowBack" style={{ color: "#fff", fontSize: 200}} />
                  <div className="pointerAnimation">
                    <img src="/img/swipe.svg" alt="Пролистывайте меню горизонтально" />
                  </div>
                  <MenuCard number="1" title="История" image="/img/galurgy-projects-020.jpg" route="/submenu/ch1" />       
                  <MenuCard number="2" title="Персоналии" image="/img/preobrazjensky-2-1024x544.jpg" route="/submenu/ch2" />   
                  <MenuCard number="3" title="Технологии" image="/img/IMG_2041_IV.jpg" route="/submenu/ch3" />

                  <MenuCard number="4" title="Проекты" image="/img/galurgy-023.jpg" route="/submenu/ch4" />
                  {/*<MenuCard number="5" title="Экспонаты" image="/img/diorama.jpg" route="/submenu/ch5" />*/}
                  <MenuCard number="5" title="Современный&nbsp;институт" image="/img/galurgy-ch6-192.jpg" route="/submenu/ch6" />
                  <MenuCard number="6" title="Направления деятельности" image="/img/directions.jpg" route="/submenu/ch7" />
                  {/*<MenuCard number="7" title="Фотогалерея" image="/img/galobook-photogallery-34.jpg" route="/submenu/ch8" />*/}
                  
                  
              </div>       
            </ScrollContainer>
        </nav>
    );
}

export default MainMenu;