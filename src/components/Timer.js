import React, {
    useEffect,
    useState
} from 'react';

import {useHistory, useLocation } from "react-router-dom";

const Timer = (props) => { //Таймаут действий пользователя для ухода на главную страницу
    let history = useHistory();
    let location = useLocation();
    
    //console.log(location.pathname);


    const timeout = 1000*60*3; //Получаем из пропсов время простоя
    //const timeout = 1000*3;

    const [message, setMes] = useState('hideMessage');
    const showMes = (stat) => {
        //let menuStatus = (message === "hideMessage") ? "showMessage" : "hideMessage";

        setMes(stat);
    }
    //let message = "";  
    

    

    useEffect(() => {

        let timer1, timer2;



        
            document.onmousemove = () => {resetTimer(); }
            document.onkeypress = () => {resetTimer();}


            function resetTimer() {
                
                //messageTimer(false);
                //showMes('hideMessage');
                clearTimeout(timer1);
                clearTimeout(timer2);
                showMes('hideMessage');
                timer1 = setTimeout(() => {
                    console.log('Timeout called!');
                    
                    //messageTimer(true);
                    if (location.pathname !== "/") {
                            showMes('showMessage');
                            //clearTimeout(timer2);
                            timer2 = setTimeout(() => {
                                //clearTimeout(mesTimer);
                                console.log('Timeout 2 called!');
                                
                                
                                history.push("/");
                                clearTimeout(timer2);
                                showMes('hideMessage');
                                //showMes('showMessage');
                            }, 10000);
                    }
                    else {
                        clearTimeout(timer1);
                        clearTimeout(timer2);
                    }
                }, timeout);
            }
            timer1 = setTimeout(() => {
                console.log('Timeout called!');
                
                //messageTimer();
            }, timeout);
            return () => { clearTimeout(timer1); }
        
    }, []);
    
    return ( 
        <div className = {"timer " + message}> 
            <div className="timer__circle">
                <div className="loader">
                    <div class="spinner">
                        <div class="circle outer"></div>
                        <div class="circle inner"></div>
                        <div class="circle main"></div>
                    </div>
                    <div className="timer__message">
                        Переход в главное меню из-за неактивности через <span className="timer__countdown"></span> секунд. <br />Коснитесь экрана, чтобы остаться на странице.
                    </div>
                </div>
                
            </div>
            

        </div>
    );
}

export default Timer;