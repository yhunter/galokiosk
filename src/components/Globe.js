//Компонент планеты для карты проектов

import React, { useRef } from 'react';

import Globe from 'react-globe.gl';
import ptsans from "../fonts/PT_Sans_Caption_Bold.json";

import {pointsData} from '../constants/mapPoints.js';

const Earth = (props) => {
    const globeEl = useRef();
    
//react globe test


    return(
        <div className="globe__planet">
            <Globe
                ref={globeEl}
                globeImageUrl="/img/earth-blue-marble.jpg"
                bumpImageUrl="/img/earth-topology.png"
                backgroundImageUrl="/img/night-sky.png"

                onGlobeReady={()=>{if (globeEl.current) {globeEl.current.pointOfView({ lat: 58, lng: 56, altitude: 2}, 4000);}  }} //Центрируем планету после загрузки
                //globeImageUrl="/img/earth-night.jpg"
                arcsData={pointsData}
                arcColor={'arcColor'}
                arcStroke={'arcStroke'}
                arcStartLat={59}
                arcStartLng={43.2}
                arcEndLat="lat"
                arcEndLng="lng"

                //pointsData={pointsData}
                //pointAltitude="pointAltitude"
                //pointColor="pointColor"
                labelDotRadius="pointRadius"
                onLabelClick={(point, event)=>{if (point.content) {props.callBack(point.pointLabel, point.content); globeEl.current.pointOfView({ lat: point.lat, lng: point.lng+10, altitude: .8}, 1000); }}}
                labelIncludeDot={true}
                onArcClick={(arc, event)=>{if (arc.content) {props.callBack(arc.pointLabel, arc.content); globeEl.current.pointOfView({ lat: arc.lat, lng: arc.lng+10, altitude: .8}, 1000); }}}
                //labelDotOrientation="top"
                labelsData={pointsData}
                labelLat="lat"
                labelLng="lng"
                labelText="pointLabel"
                labelColor="color"
                labelSize="labelSize"
                labelDotOrientation="labelDotOrientation"
                labelTypeFace={ptsans}
                labelAltitude="labelAltitude"
                
                
                //labelTypeFace="https://github.com/mrdoob/three.js/blob/dev/examples/fonts/helvetiker_regular.typeface.json"
                
                />
        </div>
    );
}

export default Earth;