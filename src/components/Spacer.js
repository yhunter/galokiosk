const Spacer = (props) => {
    return (
        <div className="spacer" style={{"height": props.space}}>&nbsp;</div>
    );
}

export default Spacer;