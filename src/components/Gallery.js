//Компонент галереи

import 'photoswipe/dist/photoswipe.css';
import 'photoswipe/dist/default-skin/default-skin.css';


import { Gallery, Item} from 'react-photoswipe-gallery';




 //Подбираем масштаб

function MyGallery(props) { //float props.scale - масштаб, array props.images - массив изображений, boolean props.caption //показывать ли описание картинки поверх миниатюры
  
  const images = props.images;

  let _scale = 130; //Значение по умолчанию
  if (props.scale!==undefined) _scale=props.scale;
  let _bg = "#012f68";
  if (props.color!==undefined) _bg=props.color;

  const listItems = images.map(( image, index ) => {//Парсим список изображений полученный в пропсах
    const aspect = image.width/image.height;
    return(
        <div className="gal__item" key={ index.toString() } style={{width: _scale*aspect+'px', flexGrow: aspect, backgroundColor: _bg}}>
          <Item
            original={image.url}
            thumbnail={image.url}
            width={image.width}
            height={image.height}
			title={image.title}
          >
            {({ ref, open, url=image.url} ) => (
				<div>
              		<img alt={image.title} ref={ref} onClick={open} src={url} />
			  		<caption>{props.caption ? image.title : ''}</caption>
			  	</div>
            )}
			
          </Item>
        </div>
    );
    }
  );

  return (
  
  <div className="gal" style={{"background-color" : _bg}}>
  <Gallery options={{"shareEl": false, "fullscreenEl": false}} > 
    {listItems}
    
    
  </Gallery>
  
  </div>
);}

export default MyGallery;