import { Fade } from 'react-slideshow-image';

const SlideShow = (props) => {

  return (

      <div className="slide-container">
        <Fade  {...props.properties}>
            {props.slides.map((item, index) => (
                <div className="each-fade" data-index={index} key={index}>
                    <div>
                        <img src={item} alt="" />
                    </div>
                </div>                        
            ))}
          
        </Fade>
      </div>

  );
};

export default SlideShow;